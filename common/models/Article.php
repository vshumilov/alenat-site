<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use common\models\BaseActiveRecord;

/**
 * Article model
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $is_deleted
 */
class Article extends BaseActiveRecord
{

    public $upload_folder = '/upload';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }
    
    public static function getList($articleGroupId = null)
    {
        $where = ['is_deleted' => false];
        
        if (!empty($articleGroupId)) {
            $where['articlegroup_id'] = $articleGroupId;
        }
        
        $articles = Article::find()->
                select(['id', 'title'])->
                where($where)->
                asArray()->all();
        
        return $articles;
    }

    public function setFile($fileName)
    {
        $file = UploadedFile::getInstanceByName($fileName);

        if (empty($file->name)) {
            return;
        }

        $this->file_url = $this->upload_folder . '/' . $file->name;
        $extArray = explode('.', $file->name);
        $this->file_ext = $extArray[count($extArray) - 1];
        $this->file_name = $file->name;
        $file->saveAs(\Yii::getAlias('@frontend') . '/web' . $this->file_url, true);
    }
    
    public function setName()
    {
        if (!empty($this->name) || empty($this->title)) {
            return;
        }
        
        $this->name = strtr($this->title, [' ' => '_']);
    }
    
    public function deleteFile()
    {
        unlink(\Yii::getAlias('@frontend') . '/web' . $this->file_url);
        
        $this->file_url = null;
        $this->file_ext = null;
        $this->file_name = null;
        
        $result = $this->save();
        
        return $result;
    }

}
