<?php

namespace common\models;

use Yii;
use common\models\BaseActiveRecord;

/**
 * Articlegroup model
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $is_deleted
 */
class Articlegroup extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articlegroup';
    }

    public static function getList($language = null)
    {
        // Get articlegroups of a language
        $articlegroups = [];
        
        $where = ['is_deleted' => false];
        
        if (!empty($language)) {
            $where['language'] = $language;
        }

        $articlegroupsList = Articlegroup::find()->
                select(['id', 'title'])->
                where($where)->
                asArray()->all();

        foreach ($articlegroupsList as $articlegroup) {
            $articlegroups[$articlegroup['id']] = $articlegroup['title'];
        }

        return $articlegroups;
    }

}
