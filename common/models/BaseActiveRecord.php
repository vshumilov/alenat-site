<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * BaseActiveRecord model
 *
 * @property integer $id
 * @property integer $deleted_at
 * @property integer $is_deleted
 */
class BaseActiveRecord extends ActiveRecord
{

    public function markDeleted()
    {
        if (empty($this->id)) {
            return false;
        }
        
        $this->is_deleted = true;
        $this->deleted_at = date('Y-m-d');
        
        $result = $this->save();
        
        return $result;
    }

}
