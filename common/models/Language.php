<?php

namespace common\models;

use Yii;
use common\models\BaseActiveRecord;

/**
 * Language model
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $is_deleted
 * @property integer $flag_url
 */
class Language extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    public static function getList()
    {
        $languages = [];

        $languagesList = Language::find()->
                select(['name', 'description'])->
                where(['is_deleted' => false])->
                asArray()->all();

        foreach ($languagesList as $language) {
            $languages[$language['name']] = $language['description'];
        }

        return $languages;
    }

}
