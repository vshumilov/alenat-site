<?php

namespace frontend\controllers;

use yii\web\Controller;
use Yii;

/**
 * Contact controller
 */
class ContactController extends Controller
{

    public function actionView()
    {
        $data = Yii::$app->api->get("organizations");
        
        if (empty($data['name'])) {
            $this->redirect("/");
            return;
        }
        
        return $this->render('../../assets/client/modules/contacts/views/detail.php', $data);
    }

}
