<?php

namespace frontend\controllers;

use yii\web\Controller;
use app\models\views\sanatoriums\SanatoriumDetailViewModel;

/**
 * Sanatoriums controller
 */
class SanatoriumsController extends Controller
{
    public function actionIndex()
    {
        return $this->render('../../assets/client/modules/sanatoriums/views/list.php');
    }
    
    public function actionView()
    {
        $data = [];
        
        $intId = \Yii::$app->getRequest()->get('id');
        
        $sanatoriumDetailViewModel = new SanatoriumDetailViewModel();
        
        $sanatoriumDetailViewModel->sanatorium = \Yii::$app->api->get("sanatoria/$intId");
        
        $sanatoriumDetailViewModel->searchSanatorium = \Yii::$app->api->get("search-sanatoria");
        
        $sanatoriumDetailViewModel->getPricelevelMap();
       
        $data['sanatoriumDetailViewModel'] = $sanatoriumDetailViewModel;
        
        return $this->render('../../assets/client/modules/sanatoriums/views/detail.php', $data);
    }
}

