<?php

namespace frontend\controllers;

use yii\web\Controller;
use Yii;

/**
 * Payment controller
 */
class PaymentController extends Controller
{

    public function actionView()
    {
        $data = Yii::$app->api->get("organizations");
        
        if (empty($data['name'])) {
            $this->redirect("/");
            return;
        }
        
        return $this->render('../../assets/client/modules/payments/views/detail.php', $data);
    }

}
