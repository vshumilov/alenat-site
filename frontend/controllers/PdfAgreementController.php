<?php

namespace frontend\controllers;

use yii\web\Controller;
use Yii;

/**
 * Pdf Agreement controller
 */
class PdfAgreementController extends Controller
{

    public function actionView()
    {
        $data = [];

        $id = Yii::$app->getRequest()->get('id');
        $code = Yii::$app->getRequest()->getQueryParam('code');

        if (empty($id) || empty($code)) {
            $this->redirect("/");
            return;
        }
        
        $params = ['code' => $code];

        $resp = Yii::$app->api->get("pdf-agreements/$id", $params);

        if (empty($resp['data']['file_name']) || empty($resp['data']['file_base64'])) {
            die("Время жизни ссылки на документ истекло");
        }

        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");
        header("Content-Type: application/pdf");
        header("Content-Disposition: inline;filename={$resp['data']['file_name']}");
        header("Content-Transfer-Encoding: binary");

        $content = base64_decode($resp['data']['file_base64']);
        $outFilePath = 'php://output';
        $file = fopen($outFilePath, 'w');
        fwrite($file, $content);
        fclose($file);

        readfile($outFilePath);
    }

}
