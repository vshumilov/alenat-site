<?php

namespace frontend\controllers;

use yii\web\Controller;
use app\models\views\agreements\AgreementConfirmViewModel;

/**
 * Agreements confirm controller
 */
class AgreementConfirmController extends Controller
{
    public function actionView()
    {
        $data = [];
        
        $id = \Yii::$app->getRequest()->get('id');
        
        if (empty($id)) {
            $this->redirect("/");
            return;
        }
        
        $agreementConfirmViewModel = new AgreementConfirmViewModel();
        
        $resp = \Yii::$app->api->get("agreements/$id", ['onlyShortData' => true]);
        
        if (!empty($resp['data'])) {
            $agreementConfirmViewModel->agreement = $resp['data'];
        }
        
        $data['agreementConfirmViewModel'] = $agreementConfirmViewModel;
        
        return $this->render('../../assets/client/modules/agreements/views/confirm.php', $data);
    }
}
