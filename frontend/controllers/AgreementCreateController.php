<?php

namespace frontend\controllers;

use yii\web\Controller;
use app\models\views\agreements\AgreementDetailViewModel;
use app\models\views\sanatoriums\SanatoriumDetailViewModel;

/**
 * Agreements create controller
 */
class AgreementCreateController extends Controller
{
    
    public function actionView()
    {
        $data = [];
        
        $id = \Yii::$app->getRequest()->get('id');
        $data['date_from'] = \Yii::$app->getRequest()->getQueryParam('date_from');
        $data['date_to'] = \Yii::$app->getRequest()->getQueryParam('date_to');
        $data['places_count'] = \Yii::$app->getRequest()->getQueryParam('places_count');
        $data['places_count_with_treatment'] = \Yii::$app->getRequest()->getQueryParam('places_count_with_treatment');
        
        if (empty($id) || empty($data['date_from']) || empty($data['date_to']) || empty($data['places_count'])) {
            $this->redirect("/");
            return;
        }
        
        $agreementDetailViewModel = new AgreementDetailViewModel();
        
        $agreementDetailViewModel->priceToApartment = \Yii::$app->api->get("price-to-apartments/$id");
        
        $sanatoriumDetailViewModel = new SanatoriumDetailViewModel();
        
        $sanatoriumDetailViewModel->sanatorium = $agreementDetailViewModel->priceToApartment['price']['sanatorium'];
        $sanatoriumDetailViewModel->searchSanatorium = \Yii::$app->api->get("search-sanatoria");
        $agreementDetailViewModel->searchSanatorium = $sanatoriumDetailViewModel->searchSanatorium;
       
        $data['sanatoriumDetailViewModel'] = $sanatoriumDetailViewModel;
        $data['agreementDetailViewModel'] = $agreementDetailViewModel;
        
        return $this->render('../../assets/client/modules/agreements/views/create.php', $data);
    }
}

