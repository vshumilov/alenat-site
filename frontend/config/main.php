<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'api' => [
            'class' => 'app\components\Api',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'bundles' => [
                //remove twitterBootstrap css
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
                //remove twitterBootstrap js
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [],
                ]
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['agreement-create', 'agreement-confirm', 'pdf-agreement'],
                    'pluralize' => true,
                    'tokens' => [
                        '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>'
                    ]
                ],
                '/' => 'sanatoriums/index',
                'sanatoriums/<id:\d+>' => 'sanatoriums/view',
                'contacts' => 'contact/view',
                'payments' => 'payment/view',
            ],
        ],
    ],
    'params' => $params,
];
