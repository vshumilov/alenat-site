<?php

namespace app\models;

use yii;
use pahanini\curl;

class AlenatApi
{

    private $token = null;
    private $url = null;
    private static $instance = null;

    private function __constrict()
    {
        
    }

    /**
     * @param string $url
     * @param string $token
     * @return self
     */
    public static function i($url = null, $token = null)
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        };

        if ($url) {
            self::$instance->url = $url;
        }

        if ($token) {
            self::$instance->token = $token;
        }
        return self::$instance;
    }

    /**
     * @param string $action
     * @param array $data
     * 
     * @return array|false
     */
    public function get($action, $data = [])
    {
        $urlParams = http_build_query($data, '', '&', PHP_QUERY_RFC3986);
        $url = $this->url . '/' . $action . '?' . $urlParams;

        if (defined("HB_PROFILER")) {
            $data['profiler_key'] = \hb\profiler\Profiler::api_block($action, $url);
            $urlParams = http_build_query($data, '', '&', PHP_QUERY_RFC3986);
            $url = $this->url . '/' . $action . '?' . $urlParams;
        }

        $request = new curl\Request();
        $request->setOptions([
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_URL => $url

        ]);

        $request->execute();

        $response = $request->getResponse();
        $decodedResponse = json_decode($response->getContent(), true);
        $code = $response->getStatusCode();

        if (!$request->isSuccessful()) {
            return ['error' => $request->getErrorMessage(), 'error_code' => $code];
        }

        if ($code >= 400) {
            return ['error' => $response->getContent(), 'error_code' => $code];
        }

        $totalCount = $response->getHeader('X-Total-Count');

        if (is_null($totalCount)) {
            return $decodedResponse;
        } else {
            return [
                'total_count' => $totalCount,
                'items' => $decodedResponse
            ];
        }
    }

    public function post($action, $data = [], $errorCode = false, $buildQuery = true)
    {
        if (defined("HB_PROFILER")) {
            $data['profiler_key'] = \hb\profiler\Profiler::api_block('POST ' . $action);
        }

        $urlParams = http_build_query($data, '', '&', PHP_QUERY_RFC3986);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        
        if ($buildQuery) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        
        curl_setopt($ch, CURLOPT_URL, $this->url . '/' . $action . '?' . $urlParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $curlResult = curl_exec($ch);
        
        $info = curl_getinfo($ch);

        if ($errorCode) {
            return $info['http_code'];
        }

        if (!$curlResult) {
            return ['error' => curl_error($ch), 'error_code' => $info['http_code']];
        }

        return $curlResult;
    }

    public function put($action, $data = [], $errorCode = true)
    {
        if (defined("HB_PROFILER")) {
            $data['profiler_key'] = \hb\profiler\Profiler::api_block('PUT ' . $action);
        }

        $urlParams = http_build_query($data, '', '&', PHP_QUERY_RFC3986);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_URL, $this->url . '/' . $action . '?' . $urlParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $curlResult = curl_exec($ch);
        $info = curl_getinfo($ch);
        
        if ($errorCode) {
            return $info['http_code'];
        }
        
        if (!$curlResult) {
            return ['error' => curl_error($ch), 'error_code' => $info['http_code']];
        }

        return $curlResult;
    }

    public function delete($action, $data = [])
    {
        if (defined("HB_PROFILER")) {
            $data['profiler_key'] = \hb\profiler\Profiler::api_block('DELETE ' . $action);
        }

        $urlParams = http_build_query($data, '', '&', PHP_QUERY_RFC3986);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_URL, $this->url . '/' . $action . '?' . $urlParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $curlResult = curl_exec($ch);
        $info = curl_getinfo($ch);
        
        if (!$curlResult) {
            return ['error' => curl_error($ch), 'error_code' => $info['http_code']];
        }

        return $info['http_code'];
    }

    public function arrayToUrl($data)
    {
        $array = array();

        if (empty($data)) {
            return $array;
        }
        
        foreach ($data as $value) {
            $array[] = urlencode(base64_encode($value));
        }
        
        return $array;
    }

}

