<?php

namespace app\models\views\sanatoriums;

use app\components\DataHelper;

class SanatoriumDetailViewModel
{

    public $sanatorium;
    public $searchSanatorium;
    public $images;
    public $mobileKey = 'Mobile';
    public $pricelevelsMap = [];
    public $dataHelper;
    protected $maxIconsCount = 7;
    public $iconsNames = [
        'suitable' => ['kids'],
        'internet' => ['wifi'],
        'healthprogram' => ['mom', 'pregnant', 'invalid'],
        'beauty' => ['water', 'beach', 'sauna', 'indoor_pool', 'pool'],
        'sport' => ['gym', 'shaping', 'sport_ground'],
        'entertainment' => ['water_slides', 'kids_room', 'play_ground', 'horse', 'water_rental', 'bicycle', 'bowling'],
    ];
    
    public function __construct()
    {
        $this->dataHelper = new DataHelper();
    }

    public function showImagesItems($imageType, $maxShowItemsCount = 3)
    {
        $i = 0;

        if (strpos($_SERVER['HTTP_USER_AGENT'], $this->mobileKey) !== false) {
            $maxShowItemsCount = 4;
        }

        foreach ($this->sanatorium['images'] as $index => $image) {
            if (!empty($imageType) && $image['image_type'] != $imageType) {
                continue;
            }

            if ($i == $maxShowItemsCount) {
                echo '<ng-include src="\'/modules/sanatoriums/views/showMoreImages.html\'" ng-repeat="imageType in [\'' . $imageType . '\']"></ng-include>';
                return;
            }

            echo '<div class="al-image-block">';
            echo '<a>';
            echo '<img class="al-image" ng-click="showImageModal(' . $index . ')" style="background-image: url(\'' . $this->dataHelper->getImageUrl($image['thumbnail_name_s']) . '\')"/>';
            echo '</a>';
            echo '</div>';

            $i++;
        }
    }
    
    public function showSearchSanatoriums($fieldName, $params = null)
    {
        if (empty($this->sanatorium[$fieldName])) {
            return;
        }

        $fields = $this->dataHelper->getIdsFromMultienum($this->sanatorium[$fieldName]);

        if (empty($fields)) {
            return;
        }

        $searchFieldName = $fieldName . 's';

        $count = 0;

        foreach ($this->searchSanatorium[$searchFieldName] as $field) {
            if (!in_array($field['id'], $fields) || !$this->isValidParams($field, $params)) {
                continue;
            }

            $count++;
            
            if ($this->maxIconsCount == $count) {
                echo '<div class="sanatoriums-detail__gallery-button"
                            ng-show="!icon' . $fieldName . 'ShowMore"
                            ng-click="icon' . $fieldName . 'ShowMore = true">
                        <a class="al-href-warning">Показать еще</a>
                     </div>';
                echo '<div ng-show="icon' . $fieldName . 'ShowMore">';
            }

            echo '<div>';

            echo '<img class="al-small-icon" src="' . $this->dataHelper->getIconUrl($field['description']) . '"/>';

            echo '<span>';

            echo $field['name'];

            echo '</span>';

            echo '</div>';
        }

        if (!$count) {
            echo '<div class="alert alert-info">Нет информации</div>';
        } else if ($count >= $this->maxIconsCount){
            echo '</div>';
            echo '<div class="sanatoriums-detail__gallery-button"
                    ng-show="icon' . $fieldName . 'ShowMore"
                    ng-click="icon' . $fieldName . 'ShowMore = false">
                    <a class="al-href-warning">Свернуть</a>
                 </div>';
        }
    }
    
    public function showSanatoriumShortInfo()
    {
        echo '<div class="sanatoriums__result_icons">';
        
        foreach ($this->iconsNames as $fieldName => $icons) {
            if (empty($this->sanatorium[$fieldName])) {
                continue;
            }

            $fields = $this->dataHelper->getIdsFromMultienum($this->sanatorium[$fieldName]);

            if (empty($fields)) {
                continue;
            }

            $searchFieldName = $fieldName . 's';

            $count = 0;
            
            foreach ($this->searchSanatorium[$searchFieldName] as $field) {
                if (!in_array($field['id'], $fields) || !in_array($field['description'], $icons)) {
                    continue;
                }

                $count++;
                
                echo '
                    <a uib-popover="' . $field['name'] . '" popover-trigger="\'mouseenter\'">
                        <img src="' . $this->dataHelper->getIconUrl($field['description']) . '"/>
                    </a>
                ';
            }
        }
        
        echo '</div>';
    }

    public function showApartments()
    {
        if (empty($this->sanatorium['apartments'])) {
            echo '<div class="alert alert-info">Нет информации</div>';
            return;
        }
        
        $fieldName = 'roomsinfostructure';

        foreach ($this->sanatorium['apartments'] as $apartment) {
            if (empty($apartment[$fieldName])) {
                continue;
            }

            echo '<dl>
                    <dt class="' . $fieldName . '">
                        <span>' . $this->pricelevelsMap[$apartment['pricelevel_id']]['name'] . ' ' . $apartment['name'] . '</span>
                    </dt>
                    <dd>';

            $roomsinfostructureIds = $this->dataHelper->getIdsFromMultienum($apartment[$fieldName]);
            
            $count = 0;

            foreach ($this->searchSanatorium[$fieldName . 's'] as $field) {
                if (!in_array($field['id'], $roomsinfostructureIds)) {
                    continue;
                }
                
                $count++;
                
                if ($this->maxIconsCount == $count) {
                    echo '<div class="sanatoriums-detail__gallery-button"
                                ng-show="!icon' . $fieldName . 'ShowMore"
                                ng-click="icon' . $fieldName . 'ShowMore = true">
                            <a class="al-href-warning">Показать еще</a>
                         </div>';
                    echo '<div ng-show="icon' . $fieldName . 'ShowMore">';
                }

                echo '<div>';

                    echo '<img class="al-small-icon" src="' . $this->dataHelper->getIconUrl($field['description']) . '"/>';

                    echo '<span>';

                        echo $field['name'];

                    echo '</span>';

                echo '</div>';
            }
            
            if ($count >= $this->maxIconsCount){
                echo '</div>';
                echo '<div class="sanatoriums-detail__gallery-button"
                        ng-show="icon' . $fieldName . 'ShowMore"
                        ng-click="icon' . $fieldName . 'ShowMore = false">
                        <a class="al-href-warning">Свернуть</a>
                     </div>';
            }

            echo '</dd>
            </dl>';
        }
    }

    protected function isValidParams($field, $params)
    {
        if (empty($params)) {
            return true;
        }

        foreach ($params as $paramFieldName => $paramValue) {
            if ($field[$paramFieldName] != $paramValue) {
                return false;
            }
        }

        return true;
    }

    public function getPricelevelMap()
    {
        if (empty($this->searchSanatorium['pricelevels'])) {
            return;
        }
        
        foreach ($this->searchSanatorium['pricelevels'] as $pricelevel) {
            $this->pricelevelsMap[$pricelevel['id']] = $pricelevel;
        }
    }
}
