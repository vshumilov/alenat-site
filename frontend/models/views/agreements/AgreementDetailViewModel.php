<?php

namespace app\models\views\agreements;

use app\components\DateHelper;
use app\components\DataHelper;

class AgreementDetailViewModel
{

    public $priceToApartment;
    public $dateHelper;
    public $searchSanatorium;
    protected $dataHelper;

    public function __construct()
    {
        $this->dateHelper = new DateHelper();
        $this->dataHelper = new DataHelper();
    }

    public function showHouses()
    {
        $this->showItems($this->priceToApartment['price']['houses'], 'Здание', 'Здания');
    }

    public function showApartment()
    {
        $this->showItems([$this->priceToApartment['apartment']], 'Номер', 'Номера');
    }

    public function showApartmentShortInfo()
    {
        $roomsinfostructureIds = $this->dataHelper->getIdsFromMultienum($this->priceToApartment['apartment']['roomsinfostructure']);
        
        echo '<div class="sanatoriums__result_icons">';

        foreach ($this->searchSanatorium['roomsinfostructures'] as $field) {
            if (!in_array($field['id'], $roomsinfostructureIds)) {
                continue;
            }

            echo '
                <a uib-popover="' . $field['name'] . '" popover-trigger="\'mouseenter\'">
                    <img src="' . $this->dataHelper->getIconUrl($field['description']) . '"/>
                </a>
            ';
        }
        
        echo '</div>';
    }

    public function calculateTotalCost($cost, $dateFrom, $dateTo, $placesCount)
    {
        if (empty($cost) || empty($dateFrom) || empty($dateTo) || empty($placesCount)) {
            return;
        }

        $dateFromDateTime = new \DateTime($dateFrom);
        $dateToDateTime = new \DateTime($dateTo);

        $interval = $dateFromDateTime->diff($dateToDateTime);

        $daysCount = (int) $interval->format('%d');

        if (empty($daysCount)) {
            return;
        }

        return $cost * $daysCount * $placesCount;
    }

    protected function showItems($items, $titleForOne, $titleForMany)
    {
        if (empty($items)) {
            return;
        }

        echo '<p>';
        echo '<span>';
        if (count($items) == 1) {
            echo $titleForOne . ': ';
        } else {
            echo $titleForMany . ': ';
        }
        echo '</span>';

        if (!empty($items[0]['pricelevel'])) {
            echo '<span>';
            echo $items[0]['pricelevel']['name'] . ' ';
            echo '</span>';
        }

        foreach ($items as $index => $item) {
            echo '<span>';
            if ($index > 0) {
                echo ', ' . $item['name'];
            } else {
                echo $item['name'];
            }
            echo '</span>';
        }
        echo '</p>';
    }

}
