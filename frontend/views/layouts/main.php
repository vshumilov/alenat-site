<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="alenatApp" ng-controller="MenuController" scrolly>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <base href="/">
    </head>
    <body>
        <?php $this->beginBody() ?>
        <nav class="navbar navbar-default al-navbar-default" ng-class="{'al-shadow': isScrolled}">
            <div class="navbar-header al-navbar-header">
                <a href="/" class="navbar-brand">
                    <img src="/modules/common/images/ic_search.svg"/>
                    Поиск санаториев
                </a>
                <div class="al-navbar-menu max-xs-hide-force">
                    <a href="/index.php/payments">Способы оплаты</a>
                    <a href="/index.php/contacts">Контакты</a>
                </div>
                <div uib-dropdown is-open="status.isopen">
                    <button id="menuButton" uib-dropdown-toggle class="close min-sm-hide-force">
                        <i class="glyphicon glyphicon-align-justify"></i>
                    </button>
                    <ul class="dropdown-menu min-sm-hide-force" uib-dropdown-menu role="menu" aria-labelledby="menuButton">
                        <li role="menuitem"><a href="/index.php/payments">Способы оплаты</a></li>
                        <li role="menuitem"><a href="/index.php/contacts">Контакты</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="main"> 
            <div ng-view class="al-main-block"><?= $content ?></div>    
        </div>
        <footer class="al-footer__background-block">
            <div class="al-footer__block">
                <ul class="al-footer__payments-types">
                    <li>
                        <img src="/modules/common/images/ic_visa.svg"/>
                    </li>
                    <li>
                        <img src="/modules/common/images/ic_master_card.svg"/>
                    </li>
                    <li>
                        <img src="/modules/common/images/ic_maestro.svg"/>
                    </li>
                    <li>
                        <img src="/modules/common/images/ic_mir.svg"/>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="/index.php/payments">Способы оплаты</a>
                    </li>
                    <li>
                        <a href="/index.php/contacts">Контакты</a>
                    </li>
                </ul>
                <p>
                    © 1996–2018, ООО "Аленат" — дешевые путевки в санатории
                </p>
                <p>
                    На сайте использованы восхитительные иконки <a class="al-href-default" href="https://icons8.com" target="_blank">icons8</a>
                </p>
            </div>
        </footer>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
