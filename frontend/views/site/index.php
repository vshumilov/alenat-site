<?php
/* @var $this yii\web\View */

$this->title = 'Tactileeng';
?>
<html lang="ru_RU" ng-app="alenatApp">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Аленат</title>
        <link href="./assets/css/app.css" rel="stylesheet" type="text/css">
    </head>
    <body ng-controller="SanatoriumsListController as vm">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <table>
                    <tr>
                        <td class="menu-button" ng-if="menuService.isShownChatsMenuButton">
                            <button class="close" ng-click="menuService.toggleChatsMenu()">
                                <i class="glyphicon glyphicon-align-justify"></i>
                            </button>
                        </td>
                        <td class="chat-name">
                            <a href="#/login" class="navbar-brand">Аленат</a>
                        </td>
                    </tr>
                </table>
            </div>      
            <ul class="nav navbar-nav navbar-right">
                <li ng-if="!authService.isAuth()">
                    <a href="#/login"><i class="glyphicon glyphicon-enter"></i> Войти</a>
                </li>
                <li ng-if="authService.isAuth()">
                    <a href="" ng-click="userService.logoutUser()"><i class="glyphicon glyphicon-exit"></i> Выйти</a>
                </li>
            </ul>
        </nav>
        <div id="main"> 
            <div ng-view></div>    
        </div>
    </body>
    <script src="./assets/js/libraries.js"></script>
    <script src="./assets/js/app.js"></script>
</html>
