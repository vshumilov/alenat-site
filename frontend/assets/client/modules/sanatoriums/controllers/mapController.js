'use strict';
alenatApp.controller('SanatoriumMapController', [
    '$scope', '$uibModalInstance', 'currentSanatorium', '$timeout',
    function ($scope, $uibModalInstance, currentSanatorium, $timeout) {
        $scope.currentSanatorium = currentSanatorium;

        $scope.markers = [
            {
                coordinates: [currentSanatorium.lat, currentSanatorium.lon], 
                properties: {balloonContent: currentSanatorium.name}, 
                options: {preset: 'islands#icon', iconColor: '#5c4281'}
            }
        ];

        $scope.__close = function (item) {
            $uibModalInstance.close(item);
        };

        $scope.__cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        
        $timeout(function() {
            angular.element(document.getElementsByClassName('modal')[0]).addClass('al-modal-full-expanded');
        }, 100);
    }]);