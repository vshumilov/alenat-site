'use strict';
alenatApp.directive('closeSelects', function () {
    return function (scope, elem, attr) {
        elem.bind("blur", function () {
            setTimeout(function () {
                if (document.activeElement.id == elem[0].id) {
                    return;
                }
                scope.closeAllSelects();
                scope.$apply();
            }, 400);
        });
    };
})
        .controller('SanatoriumsListController', [
            '$scope', '$timeout', 'sanatoriumService', 'countryService', 'cityService', 'imageService', 'dateService', 'priceService',
            function ($scope, $timeout, sanatoriumService, countryService, cityService, imageService, dateService, priceService) {
                var vm = this;

                $scope.searchForm = {};
                $scope.sanatoriumPrices = [];
                $scope.foundSanatoriums = [];
                $scope.foundCountries = [];
                $scope.treatments = [];
                $scope.selectedSanatorium = null;
                $scope.sanatoriumService = sanatoriumService;
                $scope.imageService = imageService;
                $scope.dateService = dateService;
                $scope.isOpenDateFromPicker = false;
                $scope.isOpenDateToPicker = false;
                $scope.isDateFromError = false;
                $scope.isDateToError = false;
                $scope.maxGuestsCount = 4;
                $scope.minAdultsCount = 1;
                $scope.minAdultsWithTreatmentCount = 0;
                $scope.minChildrenCount = 0;
                $scope.placesCount = angular.copy($scope.minAdultsCount);
                $scope.placesCountWithTreatment = angular.copy($scope.minAdultsWithTreatmentCount);

                var currentDate = new Date();

                vm.init = function () {
                    $scope.searchForm.city = 'Урал';//TODO: remove when all sanatoriumPrices will be ready
                    $scope.searchForm.guests = {};
                    $scope.searchForm.guests.adultsCount = angular.copy($scope.minAdultsCount);
                    $scope.searchForm.guests.childrenCount = angular.copy($scope.minChildrenCount);
                    $scope.searchForm.guests.adultsWithTreatmentCount = angular.copy($scope.minAdultsWithTreatmentCount);
                    $scope.searchForm.guests.childrenWithTreatmentCount = angular.copy($scope.minChildrenCount);
                    $scope.searchForm.for_children = {};
                    $scope.searchForm.entertainment = {};
                    
                    vm.setSeletedGuestsTitle();

                    $scope.dateFromOptions = {
                        formatYear: 'yyyy',
                        maxDate: new Date(currentDate.getFullYear() + 2, 5, 22), //TODO set last price date
                        minDate: currentDate,
                        startingDay: 1,
                        showWeeks: false
                    };

                    $scope.dateToOptions = {
                        formatYear: 'yyyy',
                        maxDate: new Date(currentDate.getFullYear() + 2, 5, 22), //TODO set last price date
                        minDate: vm.getMinDateTo(),
                        startingDay: 1,
                        showWeeks: false
                    };

                    sanatoriumService.getSearchSanatorium();
                };
                
                vm.getMinDateTo = function () {
                    var date = angular.copy(currentDate);
                    
                    if ($scope.searchForm.date_from != null) {
                        date = angular.copy($scope.searchForm.date_from);
                    }
                    
                    date.setDate(date.getDate() + 1);
                    
                    return date;
                };

                $scope.openDateFromPicker = function () {
                    $scope.isOpenDateFromPicker = true;
                };

                $scope.openDateToPicker = function () {
                    $scope.dateToOptions.minDate = vm.getMinDateTo();
                    $scope.isOpenDateToPicker = true;
                };

                $scope.sanatoriumsAutocomplete = function () {
                    if (!$scope.searchForm.name) {
                        return;
                    }

                    var request = sanatoriumService.getSanatoriumsByName($scope.searchForm.name);

                    if (!request) {
                        return;
                    }

                    request.then(function (resp) {
                        $scope.foundSanatoriums = resp.data;
                    });
                };

                $scope.selectSanatorium = function (sanatorium) {
                    $scope.searchForm.id = sanatorium.id;
                    $scope.searchForm.name = sanatorium.name;

                    if (sanatorium.country) {
                        $scope.searchForm.country = sanatorium.country.name;
                        $scope.searchForm.country_id = sanatorium.country.id;
                    }

                    if (sanatorium.city) {
                        $scope.searchForm.city = sanatorium.city.name;
                        $scope.searchForm.city_id = sanatorium.city.id;
                    }

                    $scope.foundSanatoriums = [];
                };

                $scope.countriesAutocomplete = function () {
                    if (!$scope.searchForm.country) {
                        return;
                    }

                    var request = countryService.getCountries($scope.searchForm.country);

                    if (!request) {
                        return;
                    }

                    request.then(function (resp) {
                        $scope.foundCountries = resp.data;
                    });
                };

                $scope.selectCountry = function (country) {
                    $scope.searchForm.country = country.name;
                    $scope.searchForm.country_id = country.id;

                    $scope.foundCountries = [];
                };

                $scope.citiesAutocomplete = function () {
                    if (!$scope.searchForm.city) {
                        return;
                    }

                    var request = cityService.getCities($scope.searchForm.city);

                    if (!request) {
                        return;
                    }

                    request.then(function (resp) {
                        $scope.foundCities = resp.data;
                    });
                };

                $scope.selectCity = function (city) {
                    $scope.searchForm.city = city.name;
                    $scope.searchForm.city_id = city.id;

                    $scope.foundCities = [];
                };

                $scope.selectMultienum = function (fieldName, item) {
                    if (!fieldName) {
                        return;
                    }

                    vm.showMultienumSelect(fieldName);

                    if (!$scope.searchForm) {
                        $scope.searchForm = {};
                    }

                    if (!$scope.searchForm[fieldName]) {
                        $scope.searchForm[fieldName] = {};
                    }

                    if ($scope.searchForm[fieldName][item.id]) {
                        delete $scope.searchForm[fieldName][item.id];
                    } else {
                        $scope.searchForm[fieldName][item.id] = item.name;
                    }

                    if (!$scope.multiemumMessages) {
                        $scope.multiemumMessages = {};
                    }

                    var count = Object.keys($scope.searchForm[fieldName]).length;

                    $scope.multiemumMessages[fieldName] = vm.getMessage(count);

                    $scope.setFocusToClosestInput(fieldName);
                };

                $scope.showMultienumSelect = function (fieldName) {
                    $timeout(function () {
                        vm.showMultienumSelect(fieldName);
                    }, 500);
                };

                vm.showMultienumSelect = function (fieldName) {
                    if (!fieldName) {
                        return;
                    }

                    if (!$scope.areShown) {
                        $scope.areShown = {};
                    }

                    $scope.areShown[fieldName] = true;
                    $scope.setFocusToClosestInput(fieldName);
                };

                vm.getMessage = function (count) {
                    if (count == 0) {
                        count = '';
                    } else if (count == 1) {
                        count = 'Выбран ' + count + ' пункт';
                    } else if (count > 1 && count <= 4) {
                        count = 'Выбрано ' + count + ' пункта';
                    } else {
                        count = 'Выбрано ' + count + ' пунктов';
                    }

                    return count;
                };

                $scope.setFocusToClosestInput = function (fieldName) {
                    if (!fieldName) {
                        return;
                    }

                    document.getElementById(fieldName).focus();
                };

                vm.toCamelCase = function (str) {
                    return str.substring(0, 1).toUpperCase() + str.substring(1, str.length);
                };

                $scope.closeAllSelects = function () {
                    $scope.foundSanatoriums = [];
                    $scope.foundCountries = [];
                    $scope.foundCities = [];

                    if ($scope.areShown) {
                        angular.forEach($scope.areShown, function (value, fieldName) {
                            $scope.areShown[fieldName] = false;
                        })
                    }
                };

                $scope.searchSanatoriums = function () {
                    $scope.checkDateFrom();
                    $scope.checkDateTo();

                    if ($scope.isDateFromError || $scope.isDateToError) {
                        return;
                    }

                    var params = vm.getSearchFormForRequest();

                    var request = sanatoriumService.getSanatoriums(params);

                    if (!request) {
                        return;
                    }

                    request.then(function (resp) {
                        var daysCount = dateService.countDays($scope.searchForm.date_from, $scope.searchForm.date_to);
                        
                        $scope.sanatoriumPrices = sanatoriumService.prepareSanatoriums(resp.data, daysCount, $scope.placesCount, $scope.placesCountWithTreatment);
                    });

                    var countRequest = sanatoriumService.getSanatoriumsTotalCount(angular.copy(params));

                    if (!countRequest) {
                        return;
                    }

                    countRequest.then(function (resp) {
                        $scope.sanatoriumPricesTotalCount = parseInt(resp.data);
                    });
                };

                vm.getSearchFormForRequest = function () {
                    var params = angular.copy($scope.searchForm);

                    params.order_by = 'cost';
                    params.order_mode = 'asc';

                    params.date_from = dateService.jsDateToDbDate(params.date_from);
                    params.date_to = dateService.jsDateToDbDate(params.date_to);
                    params.places_count = vm.countPlaces(params.guests) + vm.countPlacesWithTreatment(params.guests);
                    
                    params.entertainment = Object.assign(params.entertainment, params.for_children);

                    delete params.guests;
                    delete params.for_children;

                    return angular.copy(params);
                };

                vm.countPlaces = function (guests) {
                    var countPlaces = guests.adultsCount + guests.childrenCount;
                    return countPlaces;
                };
                
                vm.countPlacesWithTreatment = function (guests) {
                    var countPlacesWithTreatment = guests.adultsWithTreatmentCount + guests.childrenWithTreatmentCount;
                    return countPlacesWithTreatment;
                };

                $scope.getMoreSanatoriums = function () {
                    var params = vm.getSearchFormForRequest();

                    var request = sanatoriumService.getSanatoriums(params, $scope.sanatoriumPrices.length);

                    if (!request) {
                        return;
                    }

                    request.then(function (resp) {
                        var daysCount = dateService.countDays($scope.searchForm.date_from, $scope.searchForm.date_to);
                        
                        $scope.sanatoriumPrices = $scope.sanatoriumPrices.concat(sanatoriumService.prepareSanatoriums(resp.data, daysCount, $scope.placesCount));
                    });
                };

                $scope.checkDateFrom = function () {
                    $scope.isDateFromError = !$scope.searchForm.date_from;

                    vm.checkCorrectDates();
                };

                $scope.checkDateTo = function () {
                    $scope.isDateToError = !$scope.searchForm.date_to;

                    vm.checkCorrectDates();
                };

                $scope.decrementAdultsCount = function () {
                    if ($scope.searchForm.guests.adultsCount > vm.getMinAdultsCount()) {
                        $scope.searchForm.guests.adultsCount--;
                    }

                    vm.setSeletedGuestsTitle();
                };

                $scope.incrementAdultsCount = function () {
                    if ($scope.searchForm.guests.adultsCount < $scope.maxGuestsCount) {
                        $scope.searchForm.guests.adultsCount++;
                    }

                    vm.setSeletedGuestsTitle();
                };
                
                $scope.decrementAdultsWithTreatmentCount = function () {
                    if ($scope.searchForm.guests.adultsWithTreatmentCount > vm.getMinAdultsCount()) {
                        $scope.searchForm.guests.adultsWithTreatmentCount--;
                    }

                    vm.setSeletedGuestsTitle();
                };

                $scope.incrementAdultsWithTreatmentCount = function () {
                    if ($scope.searchForm.guests.adultsWithTreatmentCount < $scope.maxGuestsCount) {
                        $scope.searchForm.guests.adultsWithTreatmentCount++;
                    }

                    vm.setSeletedGuestsTitle();
                };

                $scope.decrementChildrenCount = function () {
                    if ($scope.searchForm.guests.childrenCount > $scope.minChildrenCount) {
                        $scope.searchForm.guests.childrenCount--;
                    }

                    vm.setSeletedGuestsTitle();
                };

                $scope.incrementChildrenCount = function () {
                    if ($scope.searchForm.guests.childrenCount < $scope.maxGuestsCount) {
                        $scope.searchForm.guests.childrenCount++;
                    }

                    vm.setSeletedGuestsTitle();
                };
                
                $scope.decrementChildrenWithTreatmentCount = function () {
                    if ($scope.searchForm.guests.childrenWithTreatmentCount > $scope.minChildrenCount) {
                        $scope.searchForm.guests.childrenWithTreatmentCount--;
                    }

                    vm.setSeletedGuestsTitle();
                };

                $scope.incrementChildrenWithTreatmentCount = function () {
                    if ($scope.searchForm.guests.childrenWithTreatmentCount < $scope.maxGuestsCount) {
                        $scope.searchForm.guests.childrenWithTreatmentCount++;
                    }

                    vm.setSeletedGuestsTitle();
                };
                
                $scope.checkMinAdultsDisabled = function () {
                    return $scope.searchForm.guests.adultsCount <= vm.getMinAdultsCount();
                };
                
                vm.getMinAdultsCount = function () {
                    if ($scope.searchForm.guests.adultsCount > 0 && $scope.searchForm.guests.adultsWithTreatmentCount > 0) {
                        return $scope.minAdultsWithTreatmentCount;
                    } else {
                        return $scope.minAdultsCount;
                    }
                };

                vm.setSeletedGuestsTitle = function () {
                    $scope.placesCount = vm.countPlaces($scope.searchForm.guests);
                    $scope.placesCountWithTreatment = vm.countPlacesWithTreatment($scope.searchForm.guests);
                    
                    var totalPlacesCount = $scope.placesCount + $scope.placesCountWithTreatment;
                    var title = 'гость';

                    if (totalPlacesCount > 1 && totalPlacesCount < 5) {
                        title = 'гостя';
                    } else if (totalPlacesCount >= 5) {
                        title = 'гостей';
                    }

                    $scope.searchForm.guests.title = totalPlacesCount + ' ' + title;
                };

                vm.checkCorrectDates = function () {
                    if (!$scope.searchForm.date_from || !$scope.searchForm.date_to) {
                        return true;
                    }

                    var dateFrom = new Date($scope.searchForm.date_from);
                    var dateTo = new Date($scope.searchForm.date_to);

                    var result = dateFrom.getTime() > dateTo.getTime();

                    if (result) {
                        $scope.searchForm.date_to = null;
                    }

                    return !result;
                };

                vm.init();
            }]);