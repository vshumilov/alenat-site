'use strict';
alenatApp.controller('SanatoriumsDetailController', [
    '$scope', '$uibModal', 'imageService', 'screenService', 'sanatoriumService',
    function ($scope, $uibModal, imageService, screenService, sanatoriumService) {
        var vm = this;

        $scope.imageService = imageService;
        $scope.sanatoriumService = sanatoriumService;
        $scope.defaultShowItemsCount = 3;
        $scope.myInterval = 5000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
        $scope.images = [];
        $scope.imagesByType = {};
        $scope.iconsByType = {};
        var slides = $scope.slides = [];
        var currIndex = 0;
        vm.imageModal = null;
        vm.animationsEnabled = true;

        $scope.getImages = function (parentId) {
            if (!parentId) {
                return;
            }

            var request = imageService.getList(parentId);

            if (!request) {
                return;
            }

            request.then(function (resp) {
                $scope.images = resp.data;

                vm.prepareSlides();
            });
        };

        vm.prepareSlides = function () {
            for (var i = 0; i < $scope.images.length; i++) {
                $scope.addSlide();
                
                if (!$scope.imagesByType[$scope.images[i].image_type]) {
                    $scope.imagesByType[$scope.images[i].image_type] = {};
                    $scope.imagesByType[$scope.images[i].image_type].showMore = false;
                    $scope.imagesByType[$scope.images[i].image_type].count = 0;
                }
                
                $scope.imagesByType[$scope.images[i].image_type].count++;
                
                $scope.images[i].counted_order_number = $scope.imagesByType[$scope.images[i].image_type].count;
            }
        };

        $scope.addSlide = function () {
            slides.push({
                image: imageService.getMediumImageUrl($scope.images[currIndex]),
                text: $scope.images[currIndex].name,
                id: currIndex++
            });
        };

        $scope.showImageModal = function (imageId) {
            vm.imageModal = $uibModal.open({
                animation: vm.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/modules/images/views/imageModal.php',
                controller: 'SanatoriumsImageModalController',
                controllerAs: 'vm',
                resolve: {
                    images: function () {
                        return $scope.images;
                    },
                    imageId: function () {
                        return imageId;
                    }
                }
            });
        };
        
        vm.init = function () {
            if (screenService.isMobileDevice) {
                $scope.defaultShowItemsCount = 4;
            }
        };
        
        vm.init();
    }]);

