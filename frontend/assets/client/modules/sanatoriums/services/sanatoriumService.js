'use strict';
alenatApp.service("sanatoriumService", [
    '$api', 'searchSanatoriumService', 'dataService', 'priceService', '$uibModal',
    function ($api, searchSanatoriumService, dataService, priceService, $uibModal) {
        var service = {};

        service.hash = 'sanatoria';
        service.searchSanatorium = {};
        service.searchSanatoriumWithMapData = {};

        service.iconsNames = {
            suitable: ['kids'],
            internet: ['wifi'],
            healthprogram: ['mom', 'pregnant', 'invalid'],
            beauty: ['water', 'beach', 'sauna', 'indoor_pool', 'pool'],
            sport: ['gym', 'shaping', 'sport_ground'],
            entertainment: ['water_slides', 'kids_room', 'play_ground', 'horse', 'water_rental', 'bicycle', 'bowling']
        };

        service.getSearchSanatorium = function () {
            var request = searchSanatoriumService.getSearchSanatorium();

            if (!request) {
                return;
            }

            request.then(function (resp) {
                service.searchSanatorium = resp.data;
                priceService.searchSanatorium = resp.data;
                service.prepareSearchSanatoriumWithMapData();
            });
        };

        service.prepareSearchSanatoriumWithMapData = function () {
            service = this;

            if (!dataService.isObject(service.searchSanatorium)) {
                return;
            }

            angular.forEach(service.searchSanatorium, function (list, key) {
                service.searchSanatoriumWithMapData[key] = dataService.listToMap(list);
            });
        };

        service.prepareSanatoriums = function (data, daysCount, placesCount, placesCountWithTreatment) {
            service = this;

            if (!dataService.isEmptyObject(data)) {
                return [];
            }

            service.citiesMap = dataService.listToMap(data.cities);
            service.countriesMap = dataService.listToMap(data.countries);
            service.housesMap = dataService.listToMap(data.houses);
            service.apartmentsMap = dataService.listToMap(data.apartments);

            for (var i = 0; i < data.sanatoriums.length; i++) {
                data.sanatoriums[i] = service.prepareSanatorium(data.sanatoriums[i]);
            }

            service.sanatoriumsMap = dataService.listToMap(data.sanatoriums);

            for (var i = 0; i < data.prices.length; i++) {
                data.prices[i] = priceService.preparePrice(
                        data.prices[i],
                        service.sanatoriumsMap,
                        service.housesMap,
                        service.apartmentsMap,
                        data.priceToHouses,
                        data.priceToApartments,
                        service.searchSanatoriumWithMapData
                        );

                var totalCost = priceService.calculateTotalCost(data.prices[i].cost, daysCount, placesCount);
                var totalCostWithTreatment = priceService.calculateTotalCost(data.prices[i].cost_with_treatment, daysCount, placesCountWithTreatment);

                data.prices[i].totalCost = totalCost + totalCostWithTreatment;
            }

            return data.prices;
        };

        service.prepareSanatorium = function (sanatorium) {
            if (!dataService.isObject(sanatorium)) {
                return;
            }

            service = this;

            sanatorium.city = service.citiesMap[sanatorium.city_id];

            sanatorium.country = service.countriesMap[sanatorium.country_id];

            sanatorium = service.prepareSanatoriumIcons(sanatorium);

            return sanatorium;
        };

        service.prepareSanatoriumIcons = function (sanatorium) {
            if (!service.searchSanatorium) {
                return sanatorium;
            }

            sanatorium.icons = [];

            angular.forEach(service.iconsNames, function (iconNames, propertyName) {
                sanatorium = service.setSanatoriumPropertyIcons(sanatorium, iconNames, propertyName);
            });

            return sanatorium;
        };

        service.setSanatoriumPropertyIcons = function (sanatorium, iconNames, propertyName) {
            if (!sanatorium[propertyName] || !iconNames || !iconNames[0]) {
                return sanatorium;
            }

            var values = sanatorium[propertyName].replace(/\^/g, '').split(',');

            if (!values || !values[0]) {
                return sanatorium;
            }

            var propertyFieldName = priceService.getPropertiesFieldName(propertyName);

            if (!service.searchSanatorium[propertyFieldName]) {
                return sanatorium;
            }

            var length = service.searchSanatorium[propertyFieldName].length;

            for (var i = 0; i < length; i++) {
                var iconName = service.searchSanatorium[propertyFieldName][i].description;
                var id = service.searchSanatorium[propertyFieldName][i].id;

                if (iconNames.indexOf(iconName) !== -1 && values.indexOf(id) !== -1) {
                    var icon = priceService.getIconObjectByName(propertyName, iconName);

                    if (icon) {
                        sanatorium.icons.push(icon);
                    }
                }
            }

            return sanatorium;
        };

        service.getPropertyNameById = function (propertyFieldName, propertyId) {
            var propertiesFieldName = priceService.getPropertiesFieldName(propertyFieldName);

            if (!service.searchSanatorium[propertiesFieldName]) {
                return;
            }

            var properties = service.searchSanatorium[propertiesFieldName];
            var length = properties.length;

            for (var i = 0; i < length; i++) {
                if (properties[i].id == propertyId) {
                    return properties[i];
                }
            }
        };

        service.getSanatoriumsByName = function (name) {
            if (!name) {
                return;
            }

            var params = {name: name, limit: 5};

            return $api.post(service.hash, params);
        };

        service.getSanatoriumsTotalCount = function (params) {
            params.count = 'id';

            return service.getSanatoriums(params)
        };

        service.getSanatoriums = function (params, offset) {
            if (!params) {
                return;
            }

            if (!offset) {
                offset = 0;
            }

            if (!params.country) {
                params.country_id = '';
            }

            if (!params.city) {
                params.city_id = '';
            }

            params.limit = 20;
            params.offset = offset;

            return $api.post(service.hash, params);
        };

        service.showSanatoriumMap = function (sanatorium) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'modules/sanatoriums/views/map.html',
                controller: 'SanatoriumMapController',
                controllerAs: 'vm',
                resolve: service.transmitData(sanatorium)
            });

            modalInstance.result
                    .then(function (currentItem) {

                    }, function () {
                    });

            modalInstance.result.catch(function (res) {

            });
        };

        service.transmitData = function (sanatorium) {
            return {
                currentSanatorium: function () {
                    return angular.copy(sanatorium);
                }
            };
        };

        return service;
    }]);


