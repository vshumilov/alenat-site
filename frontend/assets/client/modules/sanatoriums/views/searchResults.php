<div class="sanatoriums__results-block">
    <div ng-if="sanatoriumPrices.length">
        <div class="sanatoriums__result" ng-repeat="sanatoriumPrice in sanatoriumPrices| orderBy:'cost' track by $index" ng-if="sanatoriumPrice.apartments.length > 0">
            <div class="al-image-block">
                <a href="/index.php/sanatoriums/{{sanatoriumPrice.sanatorium.int_id}}" target="_blank">
                    <img class="al-image" style="background-image: url('{{imageService.getSmallImageUrl(sanatoriumPrice.sanatorium.thumbnail_name_s)}}')"/>
                </a>
            </div>
            <div class="sanatoriums__result-description-block">
                <a href="/index.php/sanatoriums/{{sanatoriumPrice.sanatorium.int_id}}" target="_blank"><h4>{{sanatoriumPrice.sanatorium.name}}</h4></a>
                <p>
                    <a ng-click="sanatoriumService.showSanatoriumMap(sanatoriumPrice.sanatorium)" class="al-map-href">{{sanatoriumPrice.sanatorium.city.name}} - Показать на карте</a>
                </p>
                <div class="sanatoriums__result-description-block-main">
                    <p>
                        Питание: {{sanatoriumPrice.foodoption.name}}
                    </p>
                    <p ng-if="sanatoriumPrice.apartments.length > 0">
                        <span ng-if="sanatoriumPrice.apartments.length == 1">Номер:</span> 
                        <span ng-if="sanatoriumPrice.apartments.length > 1">Номера:</span> 
                        <span>{{sanatoriumPrice.pricelevel.name}}</span>
                        <span ng-repeat="apartment in sanatoriumPrice.apartments| orderBy:'name' track by $index">
                            {{$index > 0 ? ', ' + apartment.name : apartment.name}}
                        </span>
                    </p>
                    <div ng-if="sanatoriumPrice.apartments[0].icons.length">
                        <p>Краткая информация о номере:</p>
                        <div class="sanatoriums__result_icons" ng-repeat="apartment in sanatoriumPrice.apartments track by $index">
                            <a uib-popover="{{icon.name}}" popover-trigger="'mouseenter'" ng-repeat="icon in apartment.icons track by $index">
                                <img src="{{icon.url}}"/>
                            </a>
                        </div>
                    </div>
                    <p>Краткая информация о санатории:</p>
                    <div class="sanatoriums__result_icons" ng-if="sanatoriumPrice.sanatorium.icons.length">
                        <a uib-popover="{{icon.name}}" popover-trigger="'mouseenter'" ng-repeat="icon in sanatoriumPrice.sanatorium.icons track by $index">
                            <img src="{{icon.url}}"/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="sanatoriums__result-button-block">
                <a href="/index.php/agreement-creates/{{sanatoriumPrice.priceToApartmentsIds[0]}}?date_from={{dateService.jsDateToDbDate(searchForm.date_from)}}&date_to={{dateService.jsDateToDbDate(searchForm.date_to)}}&places_count={{placesCount}}&places_count_with_treatment={{placesCountWithTreatment}}" target="_blank" class="btn btn-lg btn-warning al-btn-phone">
                    Забронировать
                </a>

                <a href="/index.php/sanatoriums/{{sanatoriumPrice.sanatorium.int_id}}" target="_blank" class="al-href-default">
                    Посмотреть подробнее
                </a>

                <h3>{{sanatoriumPrice.totalCost}} руб.</h3>
                <small><nobr>цена 1-го койко/дня</nobr>&nbsp;<nobr>{{sanatoriumPrice.cost}} руб.</nobr></small>
                <small><nobr>цена 1-го койко/дня с лечением</nobr>&nbsp;<nobr>{{sanatoriumPrice.cost_with_treatment}} руб.</nobr></small>
            </div>
        </div>
        <div ng-if="sanatoriumPricesTotalCount > sanatoriumPrices.length" class="sanatoriums__results-more-button">
            <button ng-click="getMoreSanatoriums()" class="btn btn-lg btn-warning">Показать еще</button>
        </div>
    </div>
    <div ng-if="sanatoriumPricesTotalCount == 0" class="sanatoriums__result-empty">
        По данному запросу ничего не найдено
    </div>
</div>