<div ng-controller="SanatoriumsListController">
<div class="sanatoriums__search-block">
    <div class="sanatoriums__search-form">
        <h1>Поиск санаториев со скидками до 60%</h1>
        <h3>Забронируйте номер по выгодной цене!</h3>

        <?php include 'modules/sanatoriums/views/searchForm.php' ?>
    </div>
</div>
<div class="sanatoriums__list-body">
    <?php include 'modules/sanatoriums/views/searchResults.php' ?>

    <div class="sanatoriums__description-block">
        <h2>Где отдохнуть и поправить здоровье?</h2>
        <h2>Хотите забронировать и купить путевку, не выходя из дома?</h2>

        <div class="sanatoriums__description-icons-block">
            <div class="sanatoriums__description-icons col-sm-4 col-xs-12">
                <img src="/modules/common/images/ic_calendar_2.svg"/>
                <p>
                    Выберите курот и даты заезда, а система найдет Вам самые подходящие для Вас санатории.
                </p>
            </div> 
            <div class="sanatoriums__description-icons col-sm-4 col-xs-12">
                <img src="/modules/common/images/ic_treatment_2.svg"/>
                <p>
                    Можно искать санатории по профилям лечения и лечебным программам.
                </p>
            </div> 
            <div class="sanatoriums__description-icons col-sm-4 col-xs-12">
                <img src="/modules/common/images/ic_card.svg"/>
                <p>
                    Никуда ходить не надо, можно забронировать и оплатить путевку прямо на сайте.
                </p>
            </div> 
        </div>
    </div>
</div>
</div>

