<div ng-controller="SanatoriumsDetailController" class="sanatoriums-detail__block" ng-init="getImages('<?php echo $sanatoriumDetailViewModel->sanatorium['id']; ?>')">
    <div class="sanatoriums-detail__carousel-block">
        <div class="sanatoriums-detail__carousel-container">
            <div class="sanatoriums-detail__title-block">
                <div class="sanatoriums-detail__name-block">
                    <h1><?php echo $sanatoriumDetailViewModel->sanatorium['name']; ?></h1>
                    <a ng-click="sanatoriumService.showSanatoriumMap({
                    name: '<?php echo htmlspecialchars($sanatoriumDetailViewModel->sanatorium['name']); ?>',
                    lat: '<?php echo $sanatoriumDetailViewModel->sanatorium['lat']; ?>',
                    lon: '<?php echo $sanatoriumDetailViewModel->sanatorium['lon']; ?>'
                    })" class="al-map-href"><?php echo $sanatoriumDetailViewModel->sanatorium['city']['name']; ?> - Показать на карте</a>
                </div>
                <div class="sanatoriums-detail__booking-block">
                    <a class="btn btn-lg btn-warning pull-right">Забронировать</a>
                </div>
            </div>
            <?php include 'modules/images/views/imagesCarousel.php' ?>

            <div class="sanatoriums-detail__gallery">
                <h3>Лечебные процедуры</h3>

                <?php $sanatoriumDetailViewModel->showImagesItems('health'); ?>
            </div>

            <div class="sanatoriums-detail__gallery">
                <h3>Размещение и интерьры</h3>

                <?php $sanatoriumDetailViewModel->showImagesItems('rooms'); ?>
            </div>

            <div class="sanatoriums-detail__gallery">
                <h3>Природа</h3>

                <?php $sanatoriumDetailViewModel->showImagesItems('nature'); ?>
            </div>

            <div class="sanatoriums-detail__gallery">
                <h3>Питание</h3>

                <?php $sanatoriumDetailViewModel->showImagesItems('food'); ?>
            </div>

            <div class="sanatoriums-detail__gallery">
                <h3>Развлечения</h3>
                
                <?php $sanatoriumDetailViewModel->showImagesItems('entertainment'); ?>
            </div>
            
            <div class="sanatoriums-detail__options">
                <h3>Номера</h3>
                
                <?php $sanatoriumDetailViewModel->showApartments(); ?>
            </div>

            <div class="sanatoriums-detail__options">
                <h3>Краткое описание санатория</h3>
                
                <dl>
                    <dt class="arrival">
                        <span>Заезд:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('arrival'); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="suitable">
                        <span>Для кого подходит:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('suitable'); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="foodoption">
                        <span>Варианты питания:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('foodoption'); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="healthprogram">
                        <span>Лечебные программы:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('healthprogram'); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="treatment">
                        <span>Профиль лечения:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('treatment'); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="beauty">
                        <span>Красота и здоровье:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('beauty'); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="kids">
                        <span>Для детей:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('entertainment', ['is_for_children' => '1']); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="entertainment">
                        <span>Развлечения:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('entertainment', ['is_for_children' => '0']); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="rent">
                        <span>Прокат:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('rent'); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="internet">
                        <span>Интернет:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('internet'); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="sport">
                        <span>Активные занятия:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('sport'); ?>
                    </dd>
                </dl>
                <dl>
                    <dt class="territory">
                        <span>Своя территория:</span>
                    </dt>
                    <dd>
                        <?php $sanatoriumDetailViewModel->showSearchSanatoriums('territory'); ?>
                    </dd>
                </dl>
            </div>
            <div class="sanatoriums-detail__description">
                <?php echo $sanatoriumDetailViewModel->sanatorium['sanatoriumText']['description']; ?>
            </div>
        </div>
    </div>
</div>


