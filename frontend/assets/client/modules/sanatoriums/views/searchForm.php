<div class="sanatoriums__search-inputs-block">
    <form name="search_form" ng-submit="searchSanatoriums()">
        <div>
            <div class="row">
                <div class="col-md-2 col-sm-12">
                    <label ng-class="{active: searchForm.city}">Город\Курорт</label>
                    <input class="sanatoriums__search-left al-pointer" ng-model="searchForm.city" ng-keyup="citiesAutocomplete()" placeholder="Город\Курорт" disabled=""/>
                    <div class="dropdown" ng-class="{open: foundCities.length}">
                        <a class="al-close" title="Закрыть" ng-show="foundCities.length" ng-click="closeAllSelects()"></a>
                        <ul class="dropdown-menu" role="menu">
                            <li role="presentation" ng-repeat="city in foundCities" ng-click="selectCity(city)">
                                <a role="menuitem" tabindex="-1" href="#" title='{{city.name}}'>
                                    {{city.name}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <label ng-class="{active: searchForm.name}">Название санатория</label>
                    <input class="sanatoriums__search-center" ng-model="searchForm.name" ng-keyup="sanatoriumsAutocomplete()" placeholder="Название санатория"/>
                    <div class="dropdown" ng-class="{open: foundSanatoriums.length}">
                        <a class="al-close" title="Закрыть" ng-show="foundSanatoriums.length" ng-click="closeAllSelects()"></a>
                        <ul class="dropdown-menu" role="menu">
                            <li role="presentation" ng-repeat="sanatorium in foundSanatoriums" ng-click="selectSanatorium(sanatorium)">
                                <a role="menuitem" tabindex="-1" href="#" title='{{sanatorium.name}}'>
                                    {{sanatorium.name}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12" ng-class="{'al-error': isDateFromError}">
                    <label ng-class="{active: isDateFromError, hide: !isDateFromError}">
                        Укажите дату заезда
                    </label>
                    <label ng-class="{active: searchForm.date_from && !isDateFromError, hide: isDateFromError}">
                        Дата заезда
                    </label>
                    <input 
                        class="sanatoriums__search-center al-pointer" 
                        uib-datepicker-popup="dd.MM.yyyy" 
                        datepicker-options="dateFromOptions" 
                        is-open="isOpenDateFromPicker"
                        show-button-bar="false"
                        name="date_from"
                        ng-click="openDateFromPicker()"
                        ng-model="searchForm.date_from" 
                        ng-blur="search_form.$submitted && checkDateFrom()"
                        ng-required="true"
                        close-text="Закрыть" 
                        alt-input-formats="['dd.MM.yyyy']"
                        placeholder="Дата заезда"
                        readonly="true"/>
                </div>
                <div class="col-md-2 col-sm-12" ng-class="{'al-error': isDateToError}">
                    <label ng-class="{active: isDateToError, hide: !isDateToError}">
                        Укажите дату выезда
                    </label>
                    <label ng-class="{active: searchForm.date_to && !isDateToError, hide: isDateToError}">
                        Дата выезда
                    </label>
                    <input 
                        class="sanatoriums__search-center al-pointer" 
                        uib-datepicker-popup="dd.MM.yyyy" 
                        datepicker-options="dateToOptions" 
                        is-open="isOpenDateToPicker"
                        show-button-bar="false"
                        name="date_to"
                        ng-click="openDateToPicker()"
                        ng-model="searchForm.date_to" 
                        ng-blur="search_form.$submitted && checkDateTo()"
                        ng-required="true"
                        close-text="Закрыть" 
                        alt-input-formats="['dd.MM.yyyy']"
                        placeholder="Дата выезда"
                        readonly="true"/>
                </div>
                <div class="col-md-3 col-sm-12">
                    <label ng-class="{active: searchForm.guests}">Гости</label>
                    <input id="guests" class="sanatoriums__search-right al-pointer" ng-model="searchForm.guests.title" ng-click="showMultienumSelect('guests')" readonly="true" placeholder="Гости" close-selects/>
                    <div class="dropdown dropdown__guests" ng-class="{open: areShown.guests}">
                        <a class="al-close" title="Закрыть" ng-show="areShown.guests" ng-click="closeAllSelects()"></a>
                        <ul class="dropdown-menu dropdown-menu__guests" role="menu">
                            <li role="presentation" ng-click="setFocusToClosestInput('guests')">
                                <div class="dropdown-menu__block">
                                    <div class="al-inline-block">
                                        <b>Взрослые</b>
                                    </div>
                                    <div class="dropdown-menu__guests-actions">
                                        <a class="al-icon-minus" 
                                           ng-click="decrementAdultsCount()"
                                           ng-disabled="checkMinAdultsDisabled()">
                                        </a>
                                        <span>{{searchForm.guests.adultsCount}}</span>
                                        <a class="al-icon-plus" 
                                           ng-click="incrementAdultsCount()" 
                                           ng-disabled="searchForm.guests.adultsCount == maxGuestsCount"></a>
                                    </div>
                                </div>
                            </li>
                            <li role="presentation" ng-click="setFocusToClosestInput('guests')">
                                <div class="dropdown-menu__block">
                                    <div class="al-inline-block">
                                        <b>Взрослые с лечением</b>
                                    </div>
                                    <div class="dropdown-menu__guests-actions">
                                        <a class="al-icon-minus" 
                                           ng-click="decrementAdultsWithTreatmentCount()"
                                           ng-disabled="checkMinAdultsDisabled()">
                                        </a>
                                        <span>{{searchForm.guests.adultsWithTreatmentCount}}</span>
                                        <a class="al-icon-plus" 
                                           ng-click="incrementAdultsWithTreatmentCount()" 
                                           ng-disabled="searchForm.guests.adultsWithTreatmentCount == maxGuestsCount"></a>
                                    </div>
                                </div>
                            </li>
                            <li role="presentation">
                                <div class="dropdown-menu__block" ng-click="setFocusToClosestInput('guests')">
                                    <div class="al-inline-block">
                                        <b>Дети</b>
                                        <small>До 17 лет</small>
                                    </div>
                                    <div class="dropdown-menu__guests-actions">
                                        <a class="al-icon-minus" 
                                           ng-click="decrementChildrenCount()"
                                           ng-disabled="searchForm.guests.childrenCount == minChildrenCount"></a>
                                        <span>{{searchForm.guests.childrenCount}}</span>
                                        <a class="al-icon-plus" 
                                           ng-click="incrementChildrenCount()"
                                           ng-disabled="searchForm.guests.childrenCount == maxGuestsCount"></a>
                                    </div>
                                </div>
                            </li>
                            <li role="presentation">
                                <div class="dropdown-menu__block" ng-click="setFocusToClosestInput('guests')">
                                    <div class="al-inline-block">
                                        <b>Дети с лечением</b>
                                        <small>До 17 лет</small>
                                    </div>
                                    <div class="dropdown-menu__guests-actions">
                                        <a class="al-icon-minus" 
                                           ng-click="decrementChildrenWithTreatmentCount()"
                                           ng-disabled="searchForm.guests.childrenWithTreatmentCount == minChildrenCount"></a>
                                        <span>{{searchForm.guests.childrenWithTreatmentCount}}</span>
                                        <a class="al-icon-plus" 
                                           ng-click="incrementChildrenWithTreatmentCount()"
                                           ng-disabled="searchForm.guests.childrenWithTreatmentCount == maxGuestsCount"></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row" ng-show="!isShownAnvancedSearch">
                <div class="col-sm-12">
                    <a class="sanatoriums__advanced-search" ng-click="isShownAnvancedSearch = true">
                        Подробный поиск
                    </a>
                </div>
            </div>
            <div class="sanatoriums__hidden-search" ng-show="isShownAnvancedSearch">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.treatment}">Профиль лечения</label>
                        <input id="treatment" class="sanatoriums__search-left" ng-model="multiemumMessages.treatment" ng-click="showMultienumSelect('treatment')" readonly="true" placeholder="Профиль лечения" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.treatment}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.treatment" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="treatment in sanatoriumService.searchSanatorium.treatments" ng-click="selectMultienum('treatment', treatment)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{treatment.name}}' class="al-checkbox" ng-class="{active: searchForm.treatment[treatment.id]}">
                                        <img ng-src="/modules/common/images/ic_{{treatment.description}}.svg"/>{{treatment.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.suitable}">Для кого подходит</label>
                        <input id="suitable" class="sanatoriums__search-center" ng-model="multiemumMessages.suitable" ng-click="showMultienumSelect('suitable')" readonly="true" placeholder="Для кого подходит" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.suitable}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.suitable" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="suitable in sanatoriumService.searchSanatorium.suitables" ng-click="selectMultienum('suitable', suitable)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{suitable.name}}' class="al-checkbox" ng-class="{active: searchForm.suitable[suitable.id]}">
                                        <img ng-src="/modules/common/images/ic_{{suitable.description}}.svg"/>{{suitable.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.healthprogram}">Лечебные программы</label>
                        <input id="healthprogram" class="sanatoriums__search-right" ng-model="multiemumMessages.healthprogram" ng-click="showMultienumSelect('healthprogram')" readonly="true" placeholder="Лечебные программы" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.healthprogram}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.healthprogram" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="healthprogram in sanatoriumService.searchSanatorium.healthprograms" ng-click="selectMultienum('healthprogram', healthprogram)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{healthprogram.name}}' class="al-checkbox" ng-class="{active: searchForm.healthprogram[healthprogram.id]}">
                                        <img ng-src="/modules/common/images/ic_{{healthprogram.description}}.svg"/>{{healthprogram.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.beauty}">Красота и здоровье</label>
                        <input id="beauty" class="sanatoriums__search-left" ng-model="multiemumMessages.beauty" ng-click="showMultienumSelect('beauty')" readonly="true" placeholder="Красота и здоровье" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.beauty}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.beauty" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="beauty in sanatoriumService.searchSanatorium.beautys" ng-click="selectMultienum('beauty', beauty)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{beauty.name}}' class="al-checkbox" ng-class="{active: searchForm.beauty[beauty.id]}">
                                        <img ng-src="/modules/common/images/ic_{{beauty.description}}.svg"/>{{beauty.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.foodoption}">Варианты питания</label>
                        <input id="foodoption" class="sanatoriums__search-center" ng-model="multiemumMessages.foodoption" ng-click="showMultienumSelect('foodoption')" readonly="true" placeholder="Варианты питания" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.foodoption}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.foodoption" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="foodoption in sanatoriumService.searchSanatorium.foodoptions" ng-click="selectMultienum('foodoption', foodoption)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{foodoption.name}}' class="al-checkbox" ng-class="{active: searchForm.foodoption[foodoption.id]}">
                                        <img ng-src="/modules/common/images/ic_{{foodoption.description}}.svg"/>{{foodoption.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.roomsinfostructure}">Инфроструктура номеров</label>
                        <input id="roomsinfostructure" class="sanatoriums__search-right" ng-model="multiemumMessages.roomsinfostructure" ng-click="showMultienumSelect('roomsinfostructure')" readonly="true" placeholder="Инфроструктура номеров" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.roomsinfostructure}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.roomsinfostructure" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="roomsinfostructure in sanatoriumService.searchSanatorium.roomsinfostructures" ng-click="selectMultienum('roomsinfostructure', roomsinfostructure)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{roomsinfostructure.name}}' class="al-checkbox" ng-class="{active: searchForm.roomsinfostructure[roomsinfostructure.id]}">
                                        <img ng-src="/modules/common/images/ic_{{roomsinfostructure.description}}.svg"/>{{roomsinfostructure.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.for_children}">Для детей</label>
                        <input id="for_children" class="sanatoriums__search-left" ng-model="multiemumMessages.for_children" ng-click="showMultienumSelect('for_children')" readonly="true" placeholder="Для детей" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.for_children}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.for_children" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="for_children in sanatoriumService.searchSanatorium.for_childrens" ng-click="selectMultienum('for_children', for_children)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{for_children.name}}' class="al-checkbox" ng-class="{active: searchForm.for_children[for_children.id]}">
                                        <img ng-src="/modules/common/images/ic_{{for_children.description}}.svg"/>{{for_children.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.sport}">Активные занятия</label>
                        <input id="sport" class="sanatoriums__search-center" ng-model="multiemumMessages.sport" ng-click="showMultienumSelect('sport')" readonly="true" placeholder="Активные занятия" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.sport}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.sport" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="sport in sanatoriumService.searchSanatorium.sports" ng-click="selectMultienum('sport', sport)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{sport.name}}' class="al-checkbox" ng-class="{active: searchForm.sport[sport.id]}">
                                        <img ng-src="/modules/common/images/ic_{{sport.description}}.svg"/>{{sport.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.rent}">Прокат</label>
                        <input id="rent" class="sanatoriums__search-right" ng-model="multiemumMessages.rent" ng-click="showMultienumSelect('rent')" readonly="true" placeholder="Прокат" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.rent}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.rent" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="rent in sanatoriumService.searchSanatorium.rents" ng-click="selectMultienum('rent', rent)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{rent.name}}' class="al-checkbox" ng-class="{active: searchForm.rent[rent.id]}">
                                        <img ng-src="/modules/common/images/ic_{{rent.description}}.svg"/>{{rent.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.entertainment}">Развлечения</label>
                        <input id="entertainment" class="sanatoriums__search-left" ng-model="multiemumMessages.entertainment" ng-click="showMultienumSelect('entertainment')" readonly="true" placeholder="Развлечения" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.entertainment}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.entertainment" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="entertainment in sanatoriumService.searchSanatorium.entertainments" ng-click="selectMultienum('entertainment', entertainment)" ng-if="entertainment.is_for_children == '0'">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{entertainment.name}}' class="al-checkbox" ng-class="{active: searchForm.entertainment[entertainment.id]}">
                                        <img ng-src="/modules/common/images/ic_{{entertainment.description}}.svg"/>{{entertainment.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.territory}">Своя территория</label>
                        <input id="territory" class="sanatoriums__search-center" ng-model="multiemumMessages.territory" ng-click="showMultienumSelect('territory')" readonly="true" placeholder="Своя территория" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.territory}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.territory" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="territory in sanatoriumService.searchSanatorium.territorys" ng-click="selectMultienum('territory', territory)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{territory.name}}' class="al-checkbox" ng-class="{active: searchForm.territory[territory.id]}">
                                        <img ng-src="/modules/common/images/ic_{{territory.description}}.svg"/>{{territory.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <label ng-class="{active: multiemumMessages.internet}">Интернет</label>
                        <input id="internet" class="sanatoriums__search-right" ng-model="multiemumMessages.internet" ng-click="showMultienumSelect('internet')" readonly="true" placeholder="Интернет" close-selects/>
                        <div class="dropdown" ng-class="{open: areShown.internet}">
                            <a class="al-close" title="Закрыть" ng-show="areShown.internet" ng-click="closeAllSelects()"></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation" ng-repeat="internet in sanatoriumService.searchSanatorium.internets" ng-click="selectMultienum('internet', internet)">
                                    <a role="menuitem" tabindex="-1" href="#" title='{{internet.name}}' class="al-checkbox" ng-class="{active: searchForm.internet[internet.id]}">
                                        <img ng-src="/modules/common/images/ic_{{internet.description}}.svg"/>{{internet.name}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a class="sanatoriums__advanced-search" ng-click="isShownAnvancedSearch = false">Скрыть подробный поиск</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="sanatoriums__search-button-block">
            <button class="btn btn-lg btn-warning" type="submit">Узнать цены</button>
        </div>
    </form>
</div>
