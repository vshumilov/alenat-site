<div ng-controller="AgreementsCreateController" class="agreements-detail__block">
    <div ng-if="stepNumber != 4" ng-click="toBackStep()" class="agreements-detail__phone-back-step">
        <a><i class="glyphicon glyphicon-chevron-left"></i>Вернуться на шаг назад</a>
    </div>
    <div class="sanatoriums__result">
        <h1>Бронирование</h1>
        <ul class="agreements-detail__steps-block">
            <li ng-class="{active: stepNumber == 1, success: stepNumber > 1}">
                <span class="agreements-detail__steps-circle">1<i ng-if="stepNumber > 1" class="glyphicon glyphicon-ok"></i></span>
                <span class="agreements-detail__steps-label">Стоимость путевки</span>
            </li>
            <li ng-class="{active: stepNumber == 2, success: stepNumber > 2}">
                <span class="agreements-detail__steps-circle">2<i ng-if="stepNumber > 2" class="glyphicon glyphicon-ok"></i></span>
                <span class="agreements-detail__steps-label">Контактная информация</span>
            </li>
            <li ng-class="{active: stepNumber == 3, success: stepNumber > 3}">
                <span class="agreements-detail__steps-circle">3<i ng-if="stepNumber > 3" class="glyphicon glyphicon-ok"></i></span>
                <span class="agreements-detail__steps-label">Подтверждение</span>
            </li>
            <li ng-class="{active: stepNumber == 4}">
                <span class="agreements-detail__steps-circle">4</span>
                <span class="agreements-detail__steps-label">Финал</span>
            </li>
        </ul>
        <div ng-show="stepNumber == 1 || (stepNumber == 4 && !createAgreementInProgress)">
            <?php include 'modules/agreements/views/create/step1.php' ?>
        </div>

        <ng-include ng-if="stepNumber == 2" src="'modules/agreements/views/create/step2.php'"></ng-include>
        
        <ng-include ng-if="stepNumber == 3" src="'modules/agreements/views/create/step3.php'"></ng-include>
        
        <ng-include ng-if="stepNumber == 4 && !createAgreementInProgress" src="'modules/agreements/views/create/step4.php'"></ng-include>
    </div>
</div>

