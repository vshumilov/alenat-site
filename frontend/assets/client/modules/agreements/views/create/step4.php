<div class="al-exspanded-table">
    <h3>Ваша контакная информация</h3>
    <div class="al-exspanded-table">
        <div class="agreements-detail__client-contacts col-xs-12 col-sm-5 row">

            <div class="form-group">
                <label>Фамилия</label>
                <div>{{createAgreementForm.last_name}}</div>
            </div>
            <div class="form-group">
                <label>Имя</label>
                <div>{{createAgreementForm.first_name}}</div>
            </div>
            <div class="form-group">
                <label>Отчество</label>
                <div>{{createAgreementForm.middle_name}}</div>
            </div>
            <div class="form-group">
                <label>Дата рождения</label>
                <div>{{createAgreementForm.birth_date}}</div>
            </div>
            <div class="form-group">
                <label>Эл. почта</label>
                <div>{{createAgreementForm.email}}</div>
            </div>
            <div>
                <label for="phone_mobile">Сотовый телефон</label>
                <div>{{createAgreementForm.phone_mobile}}</div>
            </div>
        </div>
    </div>
</div>