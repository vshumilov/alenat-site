<form ng-submit="toNextStep()"  name="create_agreement_code_form">
    <div class="al-exspanded-table">
        <div class="agreements-detail__client-contacts col-xs-12 col-sm-5 row">
            <div ng-if="showSendCodeMessage && !isValidCode" class="form-group alert alert-success">
                На Ваш номер сотового телефона отправлен код подтверждения заказа.<br/>
                Введите его в поле ввода ниже
            </div>
            <div ng-if="isValidCode" class="form-group alert alert-success">
                SMS код верный, нажмите кнопку Далее
            </div>
            <div ng-if="showSendCodeMessage === false && !isValidCode" class="form-group alert alert-danger">
                Ошибка отправки кода подтверждения заказа
            </div>
            <div class="form-group">
                <label for="code">SMS код подтверждения</label>
                <div class="al-exspanded-table">
                    <div class="col-xs-12 col-sm-6 row">
                        <input 
                            id="code" 
                            name="code" 
                            ng-model="createAgreementForm.code" 
                            ng-keyup="validateCode()" 
                            ng-blur="showCodeError = codeError" 
                            ui-mask-placeholder 
                            ui-mask-placeholder-char="space"
                            ui-mask="9999"
                            type="text" 
                            class="form-control" 
                            placeholder="SMS код подтверждения"
                            autocomplete="off"
                            ng-disabled="showSendCodeMessage === false && !isValidCode"
                            required/>
                    </div>
                    <div class="col-xs-12 col-sm-6 row agreements-detail__repeat-button-block">
                        <a ng-if="canSendCodeAgain && !isValidCode" ng-click="sendSmsCode()" class="al-href-default">
                            Повторить отправку
                        </a>
                    </div>
                </div>
                <div>
                    <small ng-show="!canSendCodeAgain && !isValidCode">
                        Отправку можно повторить через {{sendSmsTimerSeconds}} сек.
                    </small>
                </div>
                <div ng-show="showCodeError" class="alert alert-danger alert-error">
                    SMS код подтверждения обязателен для заполнения
                </div>
                <div ng-show="!showCodeError && isValidCode === false" class="alert alert-danger alert-error">
                    Неверный SMS код
                </div>
            </div>
        </div>
    </div>
    <div class="agreements-detail__buttons-block col-xs-12 col-sm-8 col-md-6 row">
        <div class="col-sm-6 form-group max-xs-hide-force">
            <a ng-click="toBackStep()" class="btn-default btn btn-lg al-btn-lg-pc">
                Назад
            </a>
        </div>
        <div class="col-sm-6 form-group">
            <button ng-disabled="!create_agreement_code_form.$valid || codeError || !isValidCode"
                    class="btn-warning al-btn-phone-bottom-fixed btn btn-lg al-btn-lg-pc">
                Далее
            </button>
        </div>
    </div>
</form>
