<form ng-submit="toNextStep()" name="create_agreement_price_form" ng-init="createAgreementForm.date_from = '<?php echo $date_from ?>';createAgreementForm.date_to = '<?php echo $date_to ?>';createAgreementForm.count_places = '<?php echo $places_count ?>';createAgreementForm.count_places_with_treatment = '<?php echo $places_count_with_treatment ?>';">
    <div class="al-exspanded-table">
        <div ng-if="stepNumber == 4" class="col-xs-12 col-sm-5 row">
            <h2>Заявка на бронь №{{agreementNumber}}</h2>
            <div class="form-group alert alert-success">
                Ваша заявка на бронь зарегистрирована.<br/>
                Информация о заявке отправлена Вам на эл. почту.<br/>
                О статусе брони мы оповестим Вас по эл. почте и по смс
            </div>
        </div>
        <div class="al-exspanded-table">
            <div class="al-image-column">
                <div class="al-exspanded-table">
                    <div class="al-image-block">
                        <a href="/index.php/sanatoriums/<?php echo $sanatoriumDetailViewModel->sanatorium['int_id']; ?>" target="_blank">
                            <img class="al-image" style="background-image: url('<?php echo $sanatoriumDetailViewModel->dataHelper->getImageUrl($sanatoriumDetailViewModel->sanatorium['thumbnail_name_s']) ?>')"/>
                        </a>
                    </div>
                </div>
                <div class="sanatoriums__result-apartment-description-block max-xs-hide-force">
                    <p>
                        Дата: с <?php echo $agreementDetailViewModel->dateHelper->dbDateToHumanDate($date_from); ?> по <?php echo $agreementDetailViewModel->dateHelper->dbDateToHumanDate($date_to); ?>
                    </p>
                    <p>
                        Питание: <?php echo $agreementDetailViewModel->priceToApartment['price']['foodoption']['name']; ?>
                    </p>
                    <?php echo $agreementDetailViewModel->showHouses(); ?>

                    <?php echo $agreementDetailViewModel->showApartment(); ?>

                    <div>
                        <p>Краткая информация о номере:</p>
                        <?php $agreementDetailViewModel->showApartmentShortInfo() ?>
                    </div>
                    <div>
                        <p>Краткая информация о санатории:</p>
                        <?php $sanatoriumDetailViewModel->showSanatoriumShortInfo() ?>
                    </div>
                </div>
            </div>
            <div class="sanatoriums__result-description-block">
                <a href="/index.php/sanatoriums/<?php echo $sanatoriumDetailViewModel->sanatorium['int_id']; ?>" target="_blank">
                    <h4><?php echo $sanatoriumDetailViewModel->sanatorium['name']; ?></h4>
                </a>
                <p>
                    <a ng-click="sanatoriumService.showSanatoriumMap({
                    name: '<?php echo htmlspecialchars($sanatoriumDetailViewModel->sanatorium['name']); ?>',
                    lat: '<?php echo $sanatoriumDetailViewModel->sanatorium['lat']; ?>',
                    lon: '<?php echo $sanatoriumDetailViewModel->sanatorium['lon']; ?>'
                    })" class="al-map-href"><?php echo $sanatoriumDetailViewModel->sanatorium['city']['name']; ?> - Показать на карте</a>
                </p>
                <div class="agreements-detail__prices-block al-pointer">
                    <div class="agreements-detail__prices-row">
                        <div class="agreements-detail__prices-title">
                            <h3>Стоимость: <?php echo ($agreementDetailViewModel->calculateTotalCost($agreementDetailViewModel->priceToApartment['price']['cost'], $date_from, $date_to, $places_count) + $agreementDetailViewModel->calculateTotalCost($agreementDetailViewModel->priceToApartment['price']['cost_with_treatment'], $date_from, $date_to, $places_count_with_treatment)); ?> руб.</h3>
                        </div>
                    </div>
                </div>
                <div class="sanatoriums__result-description-block-main">
                    <p>Цена 1-го койко/дня без лечения: <?php echo $agreementDetailViewModel->priceToApartment['price']['cost']; ?> руб.</p>
                    <p>Цена 1-го койко/дня с лечением: <?php echo $agreementDetailViewModel->priceToApartment['price']['cost_with_treatment']; ?> руб.</p>
                    <p>Цена лечения: <?php echo $agreementDetailViewModel->priceToApartment['price']['treatment_cost']; ?> руб.</p>
                    <p>Цена питания: <?php echo $agreementDetailViewModel->priceToApartment['price']['food_cost']; ?> руб.</p>
                    <p>Цена проживания: <?php echo $agreementDetailViewModel->priceToApartment['price']['accommodation_cost']; ?> руб.</p>
                    <p>Кол-во гостей без лечения: <?php echo $places_count ?></p>
                    <p>Кол-во гостей с лечением: <?php echo $places_count_with_treatment ?></p>

                    <div class="sanatoriums__result-apartment-description-block min-sm-hide-force">
                        <p>
                            Дата: с <?php echo $agreementDetailViewModel->dateHelper->dbDateToHumanDate($date_from); ?> по <?php echo $agreementDetailViewModel->dateHelper->dbDateToHumanDate($date_to); ?>
                        </p>
                        <p>
                            Питание: <?php echo $agreementDetailViewModel->priceToApartment['price']['foodoption']['name']; ?>
                        </p>
                        <?php echo $agreementDetailViewModel->showHouses(); ?>

                        <?php echo $agreementDetailViewModel->showApartment(); ?>

                        <div>
                            <p>Краткая информация о номере:</p>
                            <?php $agreementDetailViewModel->showApartmentShortInfo() ?>
                        </div>
                        <div>
                            <p>Краткая информация о санатории:</p>
                            <?php $sanatoriumDetailViewModel->showSanatoriumShortInfo() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div ng-if="stepNumber == 1" class="agreements-detail__buttons-block col-xs-12 col-sm-8 col-md-6 row">
            <div class="col-sm-6 form-group max-xs-hide-force">
                <a ng-click="toBackStep()" class="btn-default btn btn-lg al-btn-lg-pc">
                    Назад
                </a>
            </div>
            <div class="col-sm-6 form-group">
                <button class="btn-warning al-btn-phone-bottom-fixed btn btn-lg al-btn-lg-pc">
                    Далее
                </button>
            </div>
        </div>
    </div>
</form>



