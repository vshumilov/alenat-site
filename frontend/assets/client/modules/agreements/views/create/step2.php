<form ng-submit="toNextStep()" name="create_agreement_contact_form">
    <div class="al-exspanded-table">
        <div class="al-exspanded-table">
            <div class="agreements-detail__client-contacts col-xs-12 col-sm-5 row">
                <div class="form-group">
                    <label for="lastName">Фамилия</label>
                    <input 
                        id="lastName" 
                        name="last_name" 
                        ng-model="createAgreementForm.last_name" 
                        ng-keyup="validateLastName()" 
                        ng-blur="showLastNameError = lastNameError" 
                        type="text" 
                        class="form-control" 
                        placeholder="Фамилия"
                        required/>
                    <div ng-show="showLastNameError" class="alert alert-danger alert-error">
                        Фамилия обязательна для заполнения
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstName">Имя</label>
                    <input 
                        id="firstName" 
                        name="first_name" 
                        ng-model="createAgreementForm.first_name" 
                        ng-keyup="validateFirstName()" 
                        ng-blur="showFirstNameError = firstNameError" 
                        type="text" 
                        class="form-control" 
                        placeholder="Имя"
                        required/>
                    <div ng-show="showFirstNameError" class="alert alert-danger alert-error">
                        Имя обязательно для заполнения
                    </div>
                </div>
                <div class="form-group">
                    <label for="middleName">Отчество</label>
                    <input 
                        id="middleName" 
                        name="middle_name" 
                        ng-model="createAgreementForm.middle_name" 
                        ng-keyup="validateMiddleName()" 
                        ng-blur="showMiddleNameError = middleNameError" 
                        type="text" 
                        class="form-control" 
                        placeholder="Отчество"
                        required/>
                    <div ng-show="showMiddleNameError" class="alert alert-danger alert-error">
                        Отчество обязательно для заполнения
                    </div>
                </div>
                <div class="form-group">
                    <label for="birthDate">Дата рождения</label>
                    <input 
                        id="birthDate" 
                        name="birthDate"
                        ng-model="createAgreementForm.birth_date" 
                        ng-keyup="validateBirthDate()"
                        ng-blur="showBirthDateError = birthDateError" 
                        type="text" 
                        class="form-control" 
                        ui-mask-placeholder 
                        ui-mask-placeholder-char="space"
                        ui-mask="99.99.9999"
                        placeholder="дд.мм.гггг"
                        required/>
                    <div ng-show="showBirthDateError" class="alert alert-danger alert-error">
                        Дата рождения заполнена некорректно
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Эл. почта</label>
                    <input 
                        id="email" 
                        name="email" 
                        ng-model="createAgreementForm.email" 
                        ng-keyup="validateEmail()" 
                        ng-blur="showEmailError = emailError"
                        type="email" 
                        class="form-control" 
                        placeholder="Эл. почта"
                        required/>
                    <small>Мы пришлем Вам договор прямо на эл. почту, и Вам не нужно будет ходить в офис</small>
                    <div ng-show="showEmailError" class="alert alert-danger alert-error">
                        Некорректный адрес эл. почты
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone_mobile">Сотовый телефон</label>
                    <input 
                        id="phone_mobile" 
                        name="phone_mobile" 
                        ng-model="createAgreementForm.phone_mobile" 
                        ng-keyup="validatePhoneMobile()" 
                        ng-change="formatPhoneMobile()"
                        ng-blur="showPhoneMobileError = phoneMobileError" 
                        ui-mask-placeholder 
                        ui-mask-placeholder-char="space"
                        ui-mask="+7 (999) 9999-999"
                        type="text" 
                        class="form-control" 
                        placeholder="Сотовый телефон"
                        required/>
                    <small>О статусе брони мы оповестим Вас по sms</small>
                    <div ng-show="showPhoneMobileError" class="alert alert-danger alert-error">
                        Сотовый телефон обязателен для заполнения
                    </div>
                </div>
            </div>
        </div>
        <div class="agreements-detail__buttons-block col-xs-12 col-sm-8 col-md-6 row">
            <div class="col-sm-6 form-group max-xs-hide-force">
                <a ng-click="toBackStep()" class="btn-default btn btn-lg al-btn-lg-pc">
                    Назад
                </a>
            </div>
            <div class="col-sm-6 form-group">
                <button ng-disabled="!isValidContactsForm(create_agreement_contact_form)"
                        class="btn-warning al-btn-phone-bottom-fixed btn btn-lg al-btn-lg-pc">
                    Далее
                </button>
            </div>
        </div>
    </div>
</form>