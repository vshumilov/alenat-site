<form ng-submit="toNextStep()" name="confirm_agreement_login_form">
    <div class="al-exspanded-table">
        <div class="al-exspanded-table">
            <div class="agreements-detail__client-contacts col-xs-12 col-sm-5 row">
                <div ng-if="showSendCodeMessage && !isValidCode" class="form-group alert alert-success">
                    На Ваш номер сотового телефона отправлен код авторизации.<br/>
                    Введите его в поле ввода ниже
                </div>
                <div ng-if="isValidCode" class="form-group alert alert-success">
                    SMS код верный, нажмите кнопку Далее
                </div>
                <div ng-if="showSendCodeMessage === false && !isValidCode" class="form-group alert alert-danger">
                    Ошибка отправки кода авторизации заказа
                </div>
                <div class="form-group">
                    <label for="phone_mobile">Сотовый телефон</label>
                    <input 
                        id="phone_mobile" 
                        name="phone_mobile" 
                        ng-model="confirmAgreementForm.phone_mobile" 
                        ng-keyup="validatePhoneMobile()" 
                        ng-change="formatPhoneMobile()"
                        ng-blur="showPhoneMobileError = phoneMobileError" 
                        ui-mask-placeholder 
                        ui-mask-placeholder-char="space"
                        ui-mask="+7 (999) 9999-999"
                        type="text" 
                        class="form-control" 
                        placeholder="Сотовый телефон"
                        required/>
                    <small>Введите телефон, указанный при бронировании путевки</small>
                    <div ng-show="showPhoneMobileError" class="alert alert-danger alert-error">
                        Сотовый телефон обязателен для заполнения
                    </div>
                </div>
                <div class="form-group">
                    <label for="code">SMS код авторизации</label>
                    <div class="al-exspanded-table">
                        <div class="col-xs-12 col-sm-6 row">
                            <input 
                                id="code" 
                                name="code" 
                                ng-model="confirmAgreementForm.code" 
                                ng-keyup="validateCode()" 
                                ng-blur="showCodeError = codeError" 
                                ui-mask-placeholder 
                                ui-mask-placeholder-char="space"
                                ui-mask="9999"
                                type="text" 
                                class="form-control" 
                                placeholder="SMS код авторизации"
                                autocomplete="off"
                                ng-disabled="showSendCodeMessage === false && !isValidCode"
                                required/>
                        </div>
                        <div class="col-xs-12 col-sm-6 row agreements-detail__repeat-button-block">
                            <a ng-if="phoneMobileError === false && canSendCodeAgain && !isValidCode" ng-click="sendSmsCode()" class="al-href-default">
                                Получить код авторизации
                            </a>
                        </div>
                    </div>
                    <div>
                        <small ng-show="phoneMobileError === false && !canSendCodeAgain && !isValidCode">
                            Отправку можно повторить через {{sendSmsTimerSeconds}} сек.
                        </small>
                    </div>
                    <div ng-show="showCodeError" class="alert alert-danger alert-error">
                        SMS код авторизации обязателен для заполнения
                    </div>
                    <div ng-show="!showCodeError && isValidCode === false" class="alert alert-danger alert-error">
                        Неверный SMS код
                    </div>
                </div>
            </div>
        </div>
        <div class="agreements-detail__buttons-block col-xs-12 col-sm-8 col-md-6 row">
            <div class="col-sm-6 form-group">
                <button ng-disabled="!isValidLoginForm(confirm_agreement_login_form)"
                        class="btn-warning al-btn-phone-bottom-fixed btn btn-lg al-btn-lg-pc">
                    Далее
                </button>
            </div>
        </div>
    </div>
</form>