<form ng-submit="toNextStep()" name="create_agreement_price_form">
    <div class="al-exspanded-table">
        <div class="col-xs-12 col-sm-5 row">
            <h2>Договор №{{agreement.system_number}}</h2>
        </div>
        <div class="al-exspanded-table">
            <div class="al-image-column">
                <div class="al-exspanded-table">
                    <div class="al-image-block">
                        <a href="/index.php/sanatoriums/{{agreement.sanatorium.id}}" target="_blank">
                            <img class="al-image" style="background-image: url('{{imageService.getSmallImageUrl(agreement.sanatorium.thumbnail_name_s)}}')"/>
                        </a>
                    </div>
                </div>
                <div class="sanatoriums__result-apartment-description-block max-xs-hide-force">
                    <p>
                        Дата: с {{agreement.start_date_tour}} по {{agreement.end_date_tour}}
                    </p>
                    <p>
                        Питание: {{agreement.tours_foodoption_name}}
                    </p>
                    <p>
                        <span>Номер:</span>
                        <span>{{agreement.price.pricelevel_name}}</span>
                        <span>{{agreement.tours_apartment_name}}</span>
                    </p>
                    <div ng-if="agreement.apartment.icons.length">
                        <p>Краткая информация о номере:</p>
                        <div class="sanatoriums__result_icons">
                            <a uib-popover="{{icon.name}}" popover-trigger="'mouseenter'" ng-repeat="icon in agreement.apartment.icons track by $index">
                                <img src="{{icon.url}}"/>
                            </a>
                        </div>
                    </div>
                    <p>Краткая информация о санатории:</p>
                    <div class="sanatoriums__result_icons">
                        <a uib-popover="{{icon.name}}" popover-trigger="'mouseenter'" ng-repeat="icon in agreement.sanatorium.icons track by $index">
                            <img src="{{icon.url}}"/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="sanatoriums__result-description-block">
                <a href="/index.php/sanatoriums/{{agreement.sanatorium.id}}" target="_blank">
                    <h4>{{agreement.sanatorium.name}}</h4>
                </a>
                <p>
                    <a ng-click="sanatoriumService.showSanatoriumMap(agreement.sanatorium)" class="al-map-href">{{agreement.sanatorium.city_name}} - Показать на карте</a>
                </p>
                <div class="agreements-detail__prices-block al-pointer">
                    <div class="agreements-detail__prices-row">
                        <div class="agreements-detail__prices-title">
                            <h3>Стоимость: {{agreement.amount}} руб.</h3>
                        </div>
                    </div>
                </div>
                <div class="sanatoriums__result-description-block-main">
                    <p>Цена 1-го койко/дня без лечения: <nobr>{{agreement.price.cost}} руб.</nobr></p>
                    <p>Цена 1-го койко/дня с лечением: <nobr>{{agreement.price.cost_with_treatment}}</nobr> руб.</p>
                    <p>Лечение: {{agreement.price.treatment_cost}} руб.</p>
                    <p>Питание: {{agreement.price.food_cost}} руб.</p>
                    <p>Проживание: {{agreement.price.accommodation_cost}} руб.</p>
                    <p>Кол-во гостей без лечения: {{agreement.count_places}}</p>
                    <p>Кол-во гостей с лечением: {{agreement.count_places_with_treatment}}</p>

                    <p class="max-xs-hide-force">
                        <a href="/index.php/pdf-agreements/{{agreement.id}}?code={{confirmAgreementForm.code}}" target="_blank">
                            <i class="glyphicon glyphicon-download-alt al-small-icon"></i>
                            <span class="al-href-default">Скачать договор в формате pdf</span>
                        </a>
                    </p>

                    <div class="sanatoriums__result-apartment-description-block min-sm-hide-force">
                        <p>
                            Дата: с {{agreement.start_date_tour}} по {{agreement.end_date_tour}}
                        </p>
                        <p>
                            Питание: {{agreement.tours_foodoption_name}}
                        </p>
                        <p>
                            <span>Номер:</span>
                            <span>{{agreement.price.pricelevel_name}}</span>
                            <span>{{agreement.tours_apartment_name}}</span>
                        </p>

                        <div ng-if="agreement.apartment.icons.length">
                            <p>Краткая информация о номере:</p>
                            <div class="sanatoriums__result_icons">
                                <a uib-popover="{{icon.name}}" popover-trigger="'mouseenter'" ng-repeat="icon in agreement.apartment.icons track by $index">
                                    <img src="{{icon.url}}"/>
                                </a>
                            </div>
                        </div>
                        <p>Краткая информация о санатории:</p>
                        <div class="sanatoriums__result_icons">
                            <a uib-popover="{{icon.name}}" popover-trigger="'mouseenter'" ng-repeat="icon in agreement.sanatorium.icons track by $index">
                                <img src="{{icon.url}}"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="al-exspanded-table">
            <h3>Ваша контакная информация</h3>
            <div class="al-exspanded-table">
                <div class="agreements-detail__client-contacts col-xs-12 col-sm-5 row">

                    <div class="form-group">
                        <label>ФИО</label>
                        <div>{{agreement.contact_name}}</div>
                    </div>
                    <div class="form-group">
                        <label>Дата рождения</label>
                        <div>{{agreement.contacts_birth_date}}</div>
                    </div>
                    <div class="form-group">
                        <label>Эл. почта</label>
                        <div>{{agreement.contacts_email}}</div>
                    </div>
                    <div class="form-group">
                        <label>Сотовый телефон</label>
                        <div>{{agreement.contacts_phone_mobile}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="agreements-detail__buttons-block col-xs-12 col-sm-8 col-md-6 row">
            <div class="col-xs-12 form-group agreements-detail__href-fixed min-sm-hide-force">
                <a href="/index.php/pdf-agreements/{{agreement.id}}?code={{confirmAgreementForm.code}}" target="_blank">
                    <i class="glyphicon glyphicon-download-alt al-small-icon"></i>
                    <span class="al-href-default">Скачать договор в формате pdf</span>
                </a>
            </div>
            <div class="col-sm-6 form-group max-xs-hide-force">
                <a ng-click="toBackStep()" class="btn-default btn btn-lg al-btn-lg-pc">
                    Назад
                </a>
            </div>
            <div class="col-sm-6 form-group">
                <button class="btn-warning al-btn-phone-bottom-fixed btn btn-lg al-btn-lg-pc">
                    Подписать договор
                </button>
            </div>
        </div>
    </div>
</form>



