<div ng-controller="AgreementsConfirmController" class="agreements-detail__block">
    <div ng-if="stepNumber == 2 || stepNumber == 3" ng-click="toBackStep()" class="agreements-detail__phone-back-step">
        <a><i class="glyphicon glyphicon-chevron-left"></i>Вернуться на шаг назад</a>
    </div>
    <div class="sanatoriums__result">
        <h1>Подтверждение путевки</h1>
        <ul class="agreements-detail__steps-block">
            <li ng-class="{active: stepNumber == 1, success: stepNumber > 1}">
                <span class="agreements-detail__steps-circle">1<i ng-if="stepNumber > 1" class="glyphicon glyphicon-ok"></i></span>
                <span class="agreements-detail__steps-label">Авторизация</span>
            </li>
            <li ng-class="{active: stepNumber == 2, success: stepNumber > 2}">
                <span class="agreements-detail__steps-circle">2<i ng-if="stepNumber > 2" class="glyphicon glyphicon-ok"></i></span>
                <span class="agreements-detail__steps-label">Подписание договора</span>
            </li>
            <li ng-class="{active: stepNumber == 3, success: stepNumber > 3}">
                <span class="agreements-detail__steps-circle">3<i ng-if="stepNumber > 3" class="glyphicon glyphicon-ok"></i></span>
                <span class="agreements-detail__steps-label">Оплата тура</span>
            </li>
            <li ng-class="{active: stepNumber == 4}">
                <span class="agreements-detail__steps-circle">4</span>
                <span class="agreements-detail__steps-label">Финал</span>
            </li>
        </ul>
        <div ng-show="stepNumber == 1">
            <?php include 'modules/agreements/views/confirm/step1.php' ?>
        </div>
        <div ng-if="stepNumber == 2">
            <?php include 'modules/agreements/views/confirm/step2.php' ?>
        </div>
    </div>
</div>

