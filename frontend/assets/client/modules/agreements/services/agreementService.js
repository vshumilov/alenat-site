'use strict';
alenatApp.service("agreementService", [
    '$api',
    function ($api) {
        var service = {};

        service.hash = 'agreements';
        
        service.create = function (params) {
            if (!service.canCreate(params)) {
                return;
            }
            
            return $api.post(service.hash, params);
        };
        
        service.getByParams = function (agreementId, code) {
            if (!agreementId || !code) {
                return;
            }
            
            var params = {};
            params.code = code;
            
            var request = angular.copy(service.hash);
            
            if (agreementId) {
                request += '/' + agreementId;
            }
            
            return $api.get(request, params);
        };
        
        service.checkCode = function (agreementId, code, phone) {
            if (!agreementId || !code || !phone) {
                return;
            }
            
            var params = {};
            params.code = code;
            params.phone = phone;
            
            var request = 'code-agreements';
            
            if (agreementId) {
                request += '/' + agreementId;
            }
            
            return $api.get(request, params);
        };
        
        service.canCreate = function (params) {
            if (!params || 
                    !params.price_to_apartment_id || 
                    !params.date_from ||
                    !params.date_to ||
                    params.count_places === null || 
                    params.count_places_with_treatment === null || 
                    !params.last_name ||
                    !params.first_name ||
                    !params.middle_name ||
                    !params.birth_date ||
                    !params.email ||
                    !params.phone_mobile) {
                return false;
            }
            
            return true;
        };

        return service;
    }]);


