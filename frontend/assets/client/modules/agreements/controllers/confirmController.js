'use strict';
alenatApp.controller('AgreementsConfirmController', [
    '$scope', 'agreementService', 'phoneService', 'smsService', 'imageService', 'sanatoriumService', 'priceService',
    function ($scope, agreementService, phoneService, smsService, imageService, sanatoriumService, priceService) {
        var vm = this;

        var firstStepNumber = 1;
        var maxStepNumber = 4;
        var smsCodeTimerDelay = 1000;
        var smsCodeTimerMaxSeconds = 30;

        $scope.stepNumber = firstStepNumber;
        $scope.imageService = imageService;
        $scope.sanatoriumService = sanatoriumService;
        $scope.phoneMobileError = null;
        $scope.canSendCodeAgain = true;
        $scope.confirmAgreementInProgress = false;
        $scope.isValidCode = null;
        $scope.agreement = null;
        $scope.confirmAgreementForm = {};

        $scope.toNextStep = function () {
            if ($scope.stepNumber < maxStepNumber) {
                $scope.stepNumber++;

                vm.afterStepChanged();
            }
        };

        $scope.toBackStep = function () {
            if ($scope.stepNumber > firstStepNumber) {
                $scope.stepNumber--;

                vm.afterStepChanged();
            }
        };

        vm.afterStepChanged = function () {
            if ($scope.stepNumber == 2) {
                vm.getAgreement();
            }
        };

        $scope.confirmAgreement = function () {
            if (!$scope.isValidCode) {
                return;
            }

            $scope.confirmAgreementInProgress = true;

            var params = angular.copy($scope.confirmAgreementForm);

            delete params.code;

            var request = agreementService.confirm(params);

            if (!request) {
                return;
            }

            request.then(function (resp) {
                $scope.isConfirmed = resp.data.data;
                $scope.confirmAgreementInProgress = false;
            }, function (resp) {
                $scope.confirmAgreementInProgress = false;
            });
        };

        $scope.sendSmsCode = function () {
            $scope.showSendCodeMessage = true;
            $scope.isValidCode = null;
            vm.startSendCodeTimer();

            var request = smsService.sendCode($scope.confirmAgreementForm.phone_mobile);

            if (!request) {
                return;
            }

            request.then(function (resp) {
                $scope.showSendCodeMessage = true;
            }, function (resp) {
                $scope.showSendCodeMessage = false;
                vm.finishSendCodeTimer();
            });
        };

        vm.startSendCodeTimer = function () {
            $scope.sendSmsTimerSeconds = smsCodeTimerMaxSeconds;
            vm.finishSendCodeTimer();
            $scope.showSendCodeMessage = true;
            $scope.canSendCodeAgain = false;

            vm.sendCodeTimer = setInterval(function () {
                $scope.sendSmsTimerSeconds--;

                if ($scope.sendSmsTimerSeconds <= 0) {
                    vm.finishSendCodeTimer();
                }

                $scope.$apply();
            }, smsCodeTimerDelay);
        };

        vm.finishSendCodeTimer = function () {
            $scope.canSendCodeAgain = true;

            if (vm.sendCodeTimer) {
                clearInterval(vm.sendCodeTimer);
            }
        };

        $scope.validatePhoneMobile = function () {
            if (!$scope.confirmAgreementForm || !$scope.confirmAgreementForm.phone_mobile) {
                $scope.phoneMobileError = true;
                return;
            }

            $scope.phoneMobileError = false;
        };

        $scope.formatPhoneMobile = function () {
            if (!$scope.confirmAgreementForm || !$scope.confirmAgreementForm.phone_mobile) {
                return;
            }

            var phoneMobile = document.getElementById('phone_mobile');

            phoneMobile.value = phoneService.formatPhone(phoneMobile.value);
        };

        $scope.validateCode = function () {
            var request = agreementService.checkCode(
                    $scope.confirmAgreementForm.agreement_id,
                    $scope.confirmAgreementForm.code,
                    $scope.confirmAgreementForm.phone_mobile
                    );

            if (!request || !$scope.showSendCodeMessage) {
                $scope.codeError = true;
                return;
            }

            $scope.codeError = false;

            request.then(function (resp) {
                $scope.isValidCode = true;
            }, function (resp) {
                $scope.isValidCode = false;
            });
        };

        $scope.isValidLoginForm = function (confirm_agreement_login_form) {
            if (!confirm_agreement_login_form.$valid ||
                    $scope.phoneMobileError) {
                return false;
            }

            return true;
        };

        vm.getAgreementId = function () {
            var urlArray = top.location.href.split('/');

            return urlArray[urlArray.length - 1].split('?')[0];
        };

        vm.getAgreement = function () {
            if (!$scope.isValidCode || $scope.agreement != null) {
                return;
            }

            var request = agreementService.getByParams(
                    $scope.confirmAgreementForm.agreement_id,
                    $scope.confirmAgreementForm.code
                    );

            if (!request) {
                return;
            }

            request.then(function (resp) {
                $scope.agreement = resp.data.data;
                
                if ($scope.agreement.apartment) {
                    $scope.agreement.apartment = priceService.prepareApartmentIcons($scope.agreement.apartment);
                }
                
                if ($scope.agreement.sanatorium) {
                    $scope.agreement.sanatorium = sanatoriumService.prepareSanatoriumIcons($scope.agreement.sanatorium);
                }
                
            }, function (resp) {

            });
        };

        $scope.init = function () {
            $scope.confirmAgreementForm.agreement_id = vm.getAgreementId();
        };

        $scope.init();
    }]);


