'use strict';
alenatApp.controller('AgreementsCreateController', [
    '$scope', 'agreementService', 'phoneService', 'smsService', 'emailService', 'dateService', 'sanatoriumService',
    function ($scope, agreementService, phoneService, smsService, emailService, dateService, sanatoriumService) {
        var vm = this;

        var firstStepNumber = 1;
        var maxStepNumber = 4;
        var smsCodeTimerDelay = 1000;
        var smsCodeTimerMaxSeconds = 30;

        $scope.stepNumber = firstStepNumber;
        $scope.sanatoriumService = sanatoriumService;
        $scope.lastNameError = false;
        $scope.canSendCodeAgain = false;
        $scope.createAgreementInProgress = false;
        $scope.isValidCode = null;
        $scope.createAgreementForm = {};

        $scope.createAgreementForm.last_name = "Касьянов";
        $scope.createAgreementForm.first_name = "Михаил";
        $scope.createAgreementForm.middle_name = "Михайлович";
        $scope.createAgreementForm.birth_date = "23111955";
        $scope.createAgreementForm.email = "vladislav.shumilov@gmail.com";
        $scope.createAgreementForm.phone_mobile = "9221234567"; //TODO: remove test data
        
        $scope.createAgreementForm.count_places = 1;
        $scope.createAgreementForm.count_places_with_treatment = 0;

        $scope.toNextStep = function () {
            if ($scope.stepNumber < maxStepNumber) {
                $scope.stepNumber++;

                if ($scope.stepNumber == 3) {
                    $scope.sendSmsCode();
                } else if ($scope.stepNumber == 4) {
                    $scope.createAgreement();
                }
            }
        };

        $scope.createAgreement = function () {
            if (!$scope.isValidCode) {
                return;
            }
            
            $scope.createAgreementInProgress = true;
            
            var params = angular.copy($scope.createAgreementForm);
            
            if (dateService.isAdultBirthDate(params.birth_date)) {
                params.birth_date = dateService.jsDateToDbDate(dateService.maskedStrDateTojsDate(params.birth_date));
            }
            
            delete params.code;

            var request = agreementService.create(params);

            if (!request) {
                return;
            }

            request.then(function (resp) {
                $scope.agreementNumber = resp.data.data;
                $scope.createAgreementInProgress = false;
            }, function (resp) {
                $scope.createAgreementInProgress = false;
            });
        };

        $scope.sendSmsCode = function () {
            $scope.showSendCodeMessage = true;
            $scope.isValidCode = null;
            vm.startSendCodeTimer();

            var request = smsService.sendCode($scope.createAgreementForm.phone_mobile);

            if (!request) {
                return;
            }

            request.then(function (resp) {
                $scope.showSendCodeMessage = true;
            }, function (resp) {
                $scope.showSendCodeMessage = false;
                vm.finishSendCodeTimer();
            });
        };

        vm.startSendCodeTimer = function () {
            $scope.sendSmsTimerSeconds = smsCodeTimerMaxSeconds;
            vm.finishSendCodeTimer();
            $scope.showSendCodeMessage = true;
            $scope.canSendCodeAgain = false;

            vm.sendCodeTimer = setInterval(function () {
                $scope.sendSmsTimerSeconds--;

                if ($scope.sendSmsTimerSeconds <= 0) {
                    vm.finishSendCodeTimer();
                }

                $scope.$apply();
            }, smsCodeTimerDelay);
        };

        vm.finishSendCodeTimer = function () {
            $scope.canSendCodeAgain = true;

            if (vm.sendCodeTimer) {
                clearInterval(vm.sendCodeTimer);
            }
        };

        $scope.toBackStep = function () {
            if ($scope.stepNumber > firstStepNumber) {
                $scope.stepNumber--;
            }
        };

        $scope.validateLastName = function () {
            if (!$scope.createAgreementForm || !$scope.createAgreementForm.last_name) {
                $scope.lastNameError = true;
                return;
            }

            $scope.lastNameError = false;
        };

        $scope.validateFirstName = function () {
            if (!$scope.createAgreementForm || !$scope.createAgreementForm.first_name) {
                $scope.firstNameError = true;
                return;
            }

            $scope.firstNameError = false;
        };

        $scope.validateMiddleName = function () {
            if (!$scope.createAgreementForm || !$scope.createAgreementForm.middle_name) {
                $scope.middleNameError = true;
                return;
            }

            $scope.middleNameError = false;
        };
        
        $scope.validateBirthDate = function () {
            if (!$scope.createAgreementForm || !dateService.isAdultBirthDate($scope.createAgreementForm.birth_date)) {
                $scope.birthDateError = true;
                return;
            }
            
            $scope.birthDateError = false;
        };

        $scope.validateEmail = function () {
            if (!$scope.createAgreementForm || !emailService.validate($scope.createAgreementForm.email)) {
                $scope.emailError = true;
                return;
            }

            $scope.emailError = false;
        };

        $scope.validatePhoneMobile = function () {
            if (!$scope.createAgreementForm || !$scope.createAgreementForm.phone_mobile) {
                $scope.phoneMobileError = true;
                return;
            }

            $scope.phoneMobileError = false;
        };

        $scope.formatPhoneMobile = function () {
            if (!$scope.createAgreementForm || !$scope.createAgreementForm.phone_mobile) {
                return;
            }

            var phoneMobile = document.getElementById('phone_mobile');

            phoneMobile.value = phoneService.formatPhone(phoneMobile.value);
        };

        $scope.validateCode = function () {
            var request = smsService.checkCode($scope.createAgreementForm.code, $scope.createAgreementForm.phone_mobile);

            if (!request || !$scope.showSendCodeMessage) {
                return;
            }

            request.then(function (resp) {
                $scope.isValidCode = true;
            }, function (resp) {
                $scope.isValidCode = false;
            });
        };

        $scope.isValidContactsForm = function (create_agreement_contact_form) {
            if (!create_agreement_contact_form.$valid ||
                    $scope.lastNameError ||
                    $scope.firstNameError ||
                    $scope.middleNameError ||
                    $scope.birthDateError ||
                    $scope.emailError ||
                    $scope.phoneMobileError) {
                return false;
            }

            return true;
        };

        vm.getPriceToApartmentId = function () {
            var urlArray = top.location.href.split('/');

            return urlArray[urlArray.length - 1].split('?')[0];
        };

        vm.init = function () {
            $scope.createAgreementForm.price_to_apartment_id = vm.getPriceToApartmentId();
        };

        vm.init();
    }]);


