'use strict';
alenatApp.service('priceService', [
    'dataService',
    function (dataService) {
        var service = this;
        
        service.searchSanatorium = {};
        
        service.iconsNames = [
            'television', 
            'conditioner', 
            'toilet', 
            'room_bath', 
            'shower', 
            'jacuzzi', 
            'dryer', 
            'freezer', 
            'child_bed', 
            'kitchen', 
            'safe', 
            'balcon'
        ];
        
        service.baseIconUrl = '/assets/images/common/images/';
        service.iconPrefix = 'ic_';
        service.iconExt = '.svg';

        service.preparePrice = function (
                price,
                parentsMap,
                housesMap,
                apartmentsMap,
                priceToHouses,
                priceToApartments,
                searchSanatoriumWithMapData
                ) {

            if (!dataService.isObject(price)) {
                return;
            }

            service = this;

            service.price = price;

            service.price.cost = parseInt(price.cost);

            if (service.canGetFoodoption(searchSanatoriumWithMapData)) {
                service.price.foodoption = searchSanatoriumWithMapData.foodoptions[service.price.foodoption_id];
            }

            if (service.canGetPricelevel(searchSanatoriumWithMapData)) {
                service.price.pricelevel = searchSanatoriumWithMapData.pricelevels[service.price.pricelevel_id];
            }

            if (dataService.isObject(parentsMap)) {
                service.price.sanatorium = parentsMap[price.parent_id];
            }

            service.prepareHouses(priceToHouses, housesMap);

            service.prepareApartments(priceToApartments, apartmentsMap, searchSanatoriumWithMapData);

            return service.price;
        };

        service.canGetFoodoption = function (searchSanatoriumWithMapData) {
            if (dataService.isObject(searchSanatoriumWithMapData.foodoptions) &&
                    searchSanatoriumWithMapData.foodoptions[service.price.foodoption_id]) {
                return true;
            }

            return false;
        };
        
        service.canGetPricelevel = function (searchSanatoriumWithMapData) {
            if (dataService.isObject(searchSanatoriumWithMapData.pricelevels) &&
                    searchSanatoriumWithMapData.pricelevels[service.price.pricelevel_id]) {
                return true;
            }

            return false;
        };

        service.prepareHouses = function (priceToHouses, housesMap) {
            service = this;

            if (!service.price) {
                return;
            }

            service.price.housesIds = [];
            service.price.houses = [];

            if (!dataService.isArray(priceToHouses) || !dataService.isObject(housesMap)) {
                return;
            }

            for (var i = 0; i < priceToHouses.length; i++) {
                if (priceToHouses[i].price_id != service.price.id) {
                    continue;
                }

                service.price.housesIds.push(priceToHouses[i].house_id);
            }

            if (!service.price.housesIds.length) {
                return;
            }

            for (var i = 0; i < service.price.housesIds.length; i++) {
                if (!housesMap[service.price.housesIds[i]]) {
                    continue;
                }

                service.price.houses.push(housesMap[service.price.housesIds[i]]);
            }
        };

        service.prepareApartments = function (priceToApartments, apartmentsMap, searchSanatoriumWithMapData) {
            service = this;

            if (!service.price) {
                return;
            }

            service.price.apartmentsIds = [];
            service.price.apartments = [];
            service.price.priceToApartmentsIds = [];

            if (!dataService.isArray(priceToApartments) || !dataService.isObject(apartmentsMap)) {
                return;
            }

            for (var i = 0; i < priceToApartments.length; i++) {
                if (priceToApartments[i].price_id != service.price.id) {
                    continue;
                }

                service.price.apartmentsIds.push(priceToApartments[i].apartment_id);
                service.price.priceToApartmentsIds.push(priceToApartments[i].id);
            }

            if (!service.price.apartmentsIds.length) {
                return;
            }

            for (var i = 0; i < service.price.apartmentsIds.length; i++) {
                if (!apartmentsMap[service.price.apartmentsIds[i]]) {
                    continue;
                }
                
                var apartment = service.prepareApartmentIcons(apartmentsMap[service.price.apartmentsIds[i]]);

                service.price.apartments.push(apartment);
            }
        };
        
        service.prepareApartmentIcons = function (apartment) {
            if (!apartment || !apartment.roomsinfostructure) {
                return apartment;
            }

            apartment.icons = [];
            
            var values = apartment.roomsinfostructure.replace(/\^/g, '').split(',');
            
            if (!values || !values[0] || !service.searchSanatorium.roomsinfostructures) {
                return apartment;
            }
            
            var length = service.searchSanatorium.roomsinfostructures.length;
            
            for (var i = 0; i < length; i++) {
                var iconName = service.searchSanatorium.roomsinfostructures[i].description;
                var id = service.searchSanatorium.roomsinfostructures[i].id;
                
                if (service.iconsNames.indexOf(iconName) !== -1 && values.indexOf(id) !== -1) {
                    var icon = service.getIconObjectByName('roomsinfostructure', iconName);

                    if (icon) {
                        apartment.icons.push(icon);
                    }
                }
            }

            return apartment;
        };
        
        service.getIconObjectByName = function (propertyFieldName, iconName) {
            if (!iconName || !propertyFieldName) {
                return;
            }

            var propertiesFieldName = service.getPropertiesFieldName(propertyFieldName);

            if (!service.searchSanatorium[propertiesFieldName]) {
                return;
            }

            var properties = service.searchSanatorium[propertiesFieldName];
            var length = properties.length;

            for (var i = 0; i < length; i++) {
                if (properties[i].description == iconName) {
                    properties[i].url = service.getIconUrl(iconName);
                    return properties[i];
                }
            }
        };
        
        service.getPropertiesFieldName = function (propertyFieldName) {
            return propertyFieldName + 's';
        };
        
        service.getIconUrl = function (iconName) {
            if (!iconName) {
                return '';
            }

            return service.baseIconUrl + service.iconPrefix + iconName + service.iconExt;
        };
        
        service.calculateTotalCost = function (cost, daysCount, placesCount) {
            if (!cost || !daysCount || !placesCount) {
                return 0;
            }
            
            return parseFloat(cost) * parseInt(daysCount) * parseInt(placesCount);
        };

        return service;

    }])

