'use strict';
alenatApp
        .directive('scrolly', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var raw = element[0];
                    
                    angular.element(window).bind('scroll', function () {
                        if (raw.scrollTop != 0) {
                            scope.isScrolled = true;
                        } else {
                            scope.isScrolled = false;
                        }
                        
                        scope.$apply();
                    });
                }
            }
        })
        .controller('MenuController', [
            '$scope',
            function ($scope) {

            }]);

