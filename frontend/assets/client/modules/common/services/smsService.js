'use strict';
alenatApp.service('smsService', [
    '$api', 'phoneService',
    function ($api, phoneService) {
        var service = this;
        
        service.hash = 'sms';
        
        service.sendCode = function (phone) {
            var phone = phoneService.formatPhone(phone);
            
            if (!phone || phone.length != phoneService.phoneMaxLength) {
                return;
            }
            
            var params = {};
            params.phone = phone;
            
            return $api.post(service.hash, params);
        };
        
        service.checkCode = function (code, phone) {
            var phone = phoneService.formatPhone(phone);
            
            if (!code || code.length < 4 || parseInt(code) == null || !phone || phone.length != phoneService.phoneMaxLength) {
                return;
            }
            
            var params = {};
            params.code = code;
            params.phone = phone;
            
            return $api.get(service.hash, params);
        };
        
        return service;
    }]);

