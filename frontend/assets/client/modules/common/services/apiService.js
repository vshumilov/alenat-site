
'use strict';
alenatApp.factory('$api', ['$http', '$q', '$cookies', 'authService', 'ROUTES',
    function ($http, $q, $cookies, authService, ROUTES) {

        function getMethodUrl(method, httpMethod, data) {
            var url = ROUTES.API_URL + method;

            if (httpMethod == "GET") {
                angular.forEach(data, function (value, key) {
                    var delimiter = "?";

                    if (url.indexOf(delimiter) !== -1) {
                        delimiter = "&";
                    }

                    url += delimiter + key + "=" + value;
                });
            }

            return url;
        }

        function request(method, httpMethod, withCredentials, context, config, data) {
            var deferred = $q.defer();

            $http(angular.extend({
                method: httpMethod,
                url: getMethodUrl(method, httpMethod, data),
                headers: {
                    'X-Device-OS': window.navigator.oscpu || window.navigator.platform || 'unknown',
                    'X-Device-Model': window.navigator.oscpu || window.navigator.platform || 'unknown',
                    'X-Device-Type': getDeviceType(),
                    'X-App-Version': window.navigator.userAgent || 'unknown',
                    'X-Device-Id': getDeviceId(),
                    Authorization: 'Bearer ' + authService.getKey()
                },
                data: data,
                withCredentials: withCredentials
            }, config)).then(function (response) {
                if (response.data.error) {
                    switch (response.status) {
                        case 401:
                            deferred.reject(response);
                            authService.setUnauthorizedState();
                            break;
                        default:
                            deferred.reject(response);
                    }
                } else {
                    deferred.resolve(response);
                }
            }, function (response) {
                if (context && angular.isFunction(context.preProcessResponse)) {
                    context.preProcessResponse(response);
                }

                switch (response.status) {
                    case 401:
                        deferred.reject(response);
                        authService.setUnauthorizedState();
                        break;
                    default:
                        deferred.reject(response);
                }
                deferred.reject(response);
            });

            return deferred.promise;
        }

        function getDeviceId() {
            var deviceIdKey = 'deviceId';

            var deviceId = $cookies.get(deviceIdKey);

            if (!deviceId) {
                deviceId = generateDeviceId();
                $cookies.put(deviceIdKey, deviceId);
            }

            return deviceId;
        }

        function generateDeviceId() {
            var deviceId = sha256(Date.now() + navigator.userAgent).substring(0, 64);

            return deviceId;
        }

        function getDeviceType() {
            var ua = navigator.userAgent.toLowerCase();
            if (/(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(ua)) {
                return 'tablet'
            } else if (/(mobi|ipod|phone|blackberry|opera mini|fennec|minimo|symbian|psp|nintendo ds|archos|skyfire|puffin|blazer|bolt|gobrowser|iris|maemo|semc|teashark|uzard)/.test(ua)) {
                return 'phone'
            } else {
                return 'desktop'
            }
        }
        ;

        function get(method, data, withCredentials, context, config) {
            return request(method, 'GET', withCredentials, context, config, data);
        }

        function del(method, data, withCredentials, context, config) {
            return request(method, 'DELETE', withCredentials, context, config, data);
        }

        function head(method, withCredentials, context, config) {
            return request(method, 'HEAD', withCredentials, context, config);
        }

        function post(method, data, withCredentials, context, config) {
            return request(method, 'POST', withCredentials, context, config, data);
        }

        function put(method, data, withCredentials, context, config) {
            return request(method, 'PUT', withCredentials, context, config, data);
        }

        function patch(method, data, withCredentials, context, config) {
            return request(method, 'PATCH', withCredentials, context, config, data);
        }

        return {
            get: get,
            delete: del,
            head: head,
            post: post,
            put: put,
            patch: patch,
            getMethodUrl: getMethodUrl
        };

    }]);
