'use strict';
alenatApp.service("dateService", [
    function () {
        var service = {};
        
        service.adultMiliseconds = 0;

        service.inlineOptions = {
            customClass: getDayClass,
            maxDate: new Date(),
            showWeeks: true
        };

        service.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(),
            startingDay: 1
        };

        service.openDatePickerModal = function () {
            service.isOpenDatePicker.opened = true;
        };

        service.isOpenDatePicker = {
            opened: false
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);

        service.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];
        
        service.strToDbDate = function (dateStr) {
            var dateArray = service.strToDateArray(dateStr);
            
            var dbDateStr = dateArray.join('-');
            
            return dbDateStr;
        };
        
        service.strToHumanDate = function (dateStr) {
            var dateArray = service.strToDateArray(dateStr);
            
            dateArray = dateArray.reverse();
            
            var dbDateStr = dateArray.join('.');
            
            return dbDateStr;
        };
        
        service.isRightLocaleDate = function (dateStr) {
            if (!dateStr || dateStr.length != 8) {
                return false;
            }
            
            var oldDay = dateStr.substring(0, 2);
            var oldMonth = dateStr.substring(2, 4);
            var oldYear = dateStr.substring(4, 8);
            
            var oldData = [oldDay, oldMonth, oldYear];
            var oldDateStr = oldData.join('.');
            
            var dateData = new Date(Date.parse([oldYear, oldMonth, oldDay].join('.')));
            
            if (oldDateStr != dateData.toLocaleDateString()) {
                return false;
            }
            
            return true;
        };
        
        //example of masked date: 19051990
        service.maskedStrDateTojsDate = function (dateStr) {
            if (!dateStr || dateStr.length != 8) {
                return false;
            }
            
            var oldDay = dateStr.substring(0, 2);
            var oldMonth = dateStr.substring(2, 4);
            var oldYear = dateStr.substring(4, 8);
            
            var date = new Date(Date.parse([oldYear, oldMonth, oldDay].join('.')));
            
            return date;
        };
        
        service.isAdultBirthDate = function (dateStr) {
            if (!service.isRightLocaleDate(dateStr)) {
                return false;
            }
            
            var date = service.maskedStrDateTojsDate(dateStr);
            var currentDate = new Date();
     
            if ((currentDate.getTime() - date.getTime()) < service.adultMiliseconds) {
                return false;
            }
            
            return true;
        };
        
        service.getAdultMiliseconds = function () {
            service.adultMiliseconds = 18 * 360 * 24 * 60 * 60 * 1000;
        };
        
        service.strToDateArray = function (dateStr) {
            var date = new Date(Date.parse(dateStr));
            
            var dateArray = [];
            dateArray[0] = date.getFullYear();
            dateArray[1] = parseInt(date.getMonth()) + 1;
            
            if (dateArray[1].toString().length == 1) {
                dateArray[1] = '0' + dateArray[1];
            }
            
            dateArray[2] = date.getDate();
            
            if (dateArray[2].toString().length == 1) {
                dateArray[2] = '0' + dateArray[2];
            }
            
            return dateArray;
        };
        
        service.jsDateToRfcDate = function (jsDate) {
            if (!jsDate) {
                jsDate = new Date();
            }
            
            var rfcDate = service.jsDateToDbDate(jsDate);
            
            rfcDate += "T" + service.jsDateToDbTime(jsDate);
            
            rfcDate += service.jsDateToGmt(jsDate);
            
            return rfcDate;
        };
        
        service.jsDateToDbDate = function (jsDate) {
            var dateArray = [];
            dateArray[0] = jsDate.getFullYear();
            dateArray[1] = service.strPad(jsDate.getMonth() + 1);
            dateArray[2] = service.strPad(jsDate.getDate());
            
            return dateArray.join("-");
        };
        
        service.jsDateToDbTime = function (jsDate) {
            var timeArray = [];
            timeArray[0] = service.strPad(jsDate.getHours());
            timeArray[1] = service.strPad(jsDate.getMinutes());
            timeArray[2] = service.strPad(jsDate.getSeconds());
            
            return timeArray.join(":");
        };
        
        service.jsDateToGmt = function (jsDate) {
            var gmt = jsDate.toString();
            var gmtKey = "GMT";
            gmt = gmt.substring(gmt.indexOf(gmtKey)+gmtKey.length);
            gmt = gmt.substring(0, gmt.indexOf(" "));
            
            return gmt;
        };
        
        service.strPad = function (number) {
            return ("0" + number).slice(-2)
        };
        
        service.countDays = function (jsDateFrom, jsDateTo) {
            return Math.round((jsDateTo.getTime() - jsDateFrom.getTime())/(24*60*60*1000));
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                    mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        function getDayClass(data) {
            var date = data.date,
                    mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < service.events.length; i++) {
                    var currentDay = new Date(service.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return service.events[i].status;
                    }
                }
            }

            return '';
        }
        
        service.getAdultMiliseconds();

        return service;
    }]);

