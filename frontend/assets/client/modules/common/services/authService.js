'use strict';
alenatApp.service('authService', ['$rootScope', '$location', '$timeout', '$cookies', 'ROUTES', 'SETTINGS',
    function ($rootScope, $location, $timeout, $cookies, ROUTES, SETTINGS) {
        var service = this;

        service.cookieKey = "gibdd";
        service.currentUserKey = "currentUser";
        service.key = null;
        service.currentUser = null;
        service.autoLogoutCountdown = null;
        service.timer = null;
        service.defaultError = 'Ошибка авторизации';

        service.tick = function () {
            if (service.autoLogoutCountdown-- > 1) {
                service.timer = $timeout(service.tick, 1000);

                if (service.autoLogoutCountdown === SETTINGS.AUTO_LOGOUT_TIMEOUT_POPUP) {
                   $location.path('/login');
                }
            } else {
                service.setUnauthorizedState();
                $timeout.cancel(service.timer);
            }
        };

        service.setAuthorizedState = function (key, user) {
            if (!key) {
                return;
            }
            
            if (service.key == null) {
                service.key = key;
                service.currentUser = user;
                $cookies.put(service.cookieKey, service.key, {'expires': 0});
                $cookies.putObject(service.currentUserKey, user, {'expires': 0});
                
                $rootScope.$emit('Authorized');
            }
            
            service.resetAutoLogoutCountdown();
            $timeout.cancel(service.timer);
            service.timer = $timeout(service.tick, 1000);

            if ($location.path() === ROUTES.PRELOGIN) {
                $location.path(ROUTES.MAIN);
            }
        };

        service.setUnauthorizedState = function () {
            $rootScope.hidePrelogin = false;

            if (service.key !== null) {
                service.key = null;
                service.currentUser = null;
                $cookies.remove(service.cookieKey);
                $cookies.remove(service.currentUserKey);
                $rootScope.$emit('Unauthorized');
            }
            $timeout.cancel(service.timer);
            $location.path(ROUTES.PRELOGIN);
        };

        service.resetAutoLogoutCountdown = function () {
            service.autoLogoutCountdown = SETTINGS.AUTO_LOGOUT_TIMEOUT;
        };
        
        service.isAuth = function () {
            return service.getKey() != null;
        };
        
        service.getKey = function () {
            var service = this;
            
            if (!service.key) {
                service.key = $cookies.get(service.cookieKey);
            }
            
            return service.key;
        };
        
        service.getCurrentUser = function () {
            var service = this;
            
            if (!service.currentUser) {
                service.currentUser = $cookies.getObject(service.currentUserKey);
            }
            
            return service.currentUser;
        };

    }]);

