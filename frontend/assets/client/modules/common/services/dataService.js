'use strict';
alenatApp.service('dataService', [
    function () {
        var service = this;
        
        service.listToMap = function (list, idFieldName) {
            var map = {};
            
            if (!list || !Array.isArray(list)) {
                return map;
            }
            
            if (!idFieldName) {
                idFieldName = 'id';
            }
            
            for (var i = 0; i < list.length; i++) {
                map[list[i][idFieldName]] = list[i];
            }
            
            return map;
        };
        
        service.isObject = function (data) {
            if (!data || (typeof data) != 'object') {
                return false;
            }
            
            return true;
        };
        
        service.isEmptyObject = function (data) {
            if (!service.isObject(data) || Object.keys(data).length == 0) {
                return false;
            }
            
            return true;
        };
        
        service.isArray = function (data) {
            if (!data || !Array.isArray(data)) {
                return false;
            }
            
            return true;
        };
        
        return service;

    }]);

