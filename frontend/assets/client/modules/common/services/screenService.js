'use strict';
alenatApp.service('screenService', [
    function () {
        var service = this;
        
        service.isMobileDevice = false;

        service.checkMobileDevice = function () {
            if (navigator.userAgent.indexOf('Mobile') !== -1) {
                service.isMobileDevice = true;
            }
        };
        
        service.init = function () {
            service.checkMobileDevice();
        };
        
        service.init();
        
        return service;

    }]);

