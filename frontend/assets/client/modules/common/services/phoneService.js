'use strict';
alenatApp.service('phoneService', [
    function () {
        var service = this;
        
        service.phoneMaxLength = 10;
        
        service.formatPhone = function (phone) {
            phone = phone.trim()
                    .replace(/\(|\)/g, '')
                    .replace(/ /g, '')
                    .replace(/\+/g, '')
                    .replace(/\-/g, '');
            
            if (phone.length <= service.phoneMaxLength) {
                return phone;
            }
            
            phone = phone.substring(phone.length - service.phoneMaxLength, phone.length);
            
            return phone;
        };
        
        return service;

    }]);

