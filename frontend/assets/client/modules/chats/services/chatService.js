'use strict';
alenatApp.service("chatService", ['$api', '$q', 'dateService', 'guidService',
    function ($api, $q, dateService, guidService) {
        var service = {};
        
        service.countMessages = 50;

        service.getChats = function () {
            return $api.get('chats');
        };
        
        service.getHistoricMessages = function (chatId, endDate) {
            return $api.get('chats/messages', {
                chatId: chatId,
                endDate: encodeURIComponent(endDate),
                count: service.countMessages
            });
        };
        
        service.getFutureMessages = function (chatId, startDate) {
            return $api.get('chats/messages', {
                chatId: chatId,
                startDate: encodeURIComponent(startDate),
                count: service.countMessages
            });
        };
        
        service.sendMessage = function (newMessage) {
            newMessage.id = guidService.generate();
            newMessage.writeDate = dateService.jsDateToRfcDate();
            
            var deferred = $q.defer();
            
            $api.post('chats/messages', [newMessage]).then(function() {
                newMessage.createDate = angular.copy(newMessage.writeDate);
                deferred.resolve(newMessage);
            }, function (response) {
                deferred.reject(response);
            });
            
            return deferred.promise;
        };
        
        service.deleteMessage = function (message) {
            return $api.delete('chats/messages', message);
        };
        
        service.recoverMessage = function (message) {
            return $api.delete('chats/messages', message);
        };
        
        service.uploadFile = function (file) {
            return $api.post('files', {file: file});
        };
        
        return service;
    }]);


