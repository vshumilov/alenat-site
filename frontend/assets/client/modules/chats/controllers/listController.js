'use strict';
alenatApp
        .directive('messagesScroll', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var raw = element[0];

                    element.bind('scroll', function () {
                        if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight && scope.messages[0]) {
                            scope.getMoreFutureMessages();
                        } else if (raw.scrollTop < 50 && scope.messages[0]) {
                            scope.getMoreHistoricMessages();
                        }
                    });
                }
            }
        })
        .controller('listChatsController', ['$scope', 'chatService', 'authService', 'dateService', '$timeout', 'menuService', '$location', '$anchorScroll',
            function ($scope, chatService, authService, dateService, $timeout, menuService, $location, $anchorScroll) {
                var vm = this;

                $scope.dateService = dateService;
                $scope.chatService = chatService;
                $scope.chats = [];
                $scope.messages = [];
                $scope.users = [];
                $scope.editorDefaultText = "Пишите сообщение...";
                $scope.editorCountRows = 1;
                vm.maxEditorCountRows = 3;
                vm.getMessagesInterval = null;
                vm.enterButtonKeyCode = 13;
                vm.deletedMessageText = "Сообщение удалено";
                vm.lastEndDate = dateService.jsDateToRfcDate();
                vm.lastStartDate = dateService.jsDateToRfcDate();
                $scope.currentUser = authService.getCurrentUser();

                $scope.newMessage = {
                    id: null,
                    chatId: null,
                    userIds: null,
                    content: $scope.editorDefaultText,
                    authorId: $scope.currentUser.id,
                    writeDate: null,
                    createDate: null,
                    deliveryDate: null,
                    readDate: null,
                    file: null,
                    type: "text" //or file
                };

                vm.emptyUser = {
                    id: null,
                    name: null
                };

                menuService.showChatsMenuButton();

                vm.maxRows = 10
                vm.totalChatCount = 0;

                vm.init = function () {
                    vm.getChats();
                };

                vm.getChats = function () {
                    chatService.getChats().then(
                            function (response) {
                                $scope.chats = response.data.data.chats;
                                $scope.users = response.data.data.users;

                                if (!$scope.currentChat) {
                                    $scope.setCurrentChat($scope.chats[0]);
                                }

                            }, function (response) {

                    });
                };

                $scope.setCurrentChat = function (chat) {
                    $scope.messages = [];
                    $scope.currentChat = chat;
                    $scope.getMoreHistoricMessages();

                    if (vm.getMessagesInterval !== null) {
                        clearInterval(vm.getMessagesInterval);
                    }

                    /*vm.getMessagesInterval = setInterval(function () {
                        $scope.getMoreFutureMessages();
                    }, 10000);*///TODO: раскомментировать
                };

                $scope.findOpponentUser = function (chat) {
                    var userId = vm.getOpponentUserId(chat);

                    var user = $scope.findUserById(userId);

                    return user;
                };

                vm.getOpponentUserId = function (chat) {
                    if (!chat || !chat.usersId) {
                        return;
                    }

                    var usersIdLength = chat.usersId.length;

                    for (var i = 0; i < usersIdLength; i++) {
                        if (chat.usersId[i] != $scope.currentUser.id) {
                            return chat.usersId[i];
                        }
                    }
                };

                $scope.findUserById = function (userId) {
                    if (!userId || $scope.users.length == 0) {
                        return vm.emptyUser;
                    }

                    if (userId == $scope.currentUser.id) {
                        return $scope.currentUser;
                    }

                    for (var i = 0; i < $scope.users.length; i++) {
                        if ($scope.users[i].id == userId) {
                            return $scope.users[i];
                        }
                    }
                };

                $scope.getMoreHistoricMessages = function () {
                    if (!$scope.currentChat || !$scope.currentChat.id) {
                        return;
                    }
                    
                    chatService.getHistoricMessages($scope.currentChat.id, vm.lastStartDate).then(
                            function (response) {
                                $scope.menuService.isShownChatsMenuForPhone = false;
                                
                                var oldCount = $scope.messages.length;
                                vm.addMessages(response.data.data);
                                
                                vm.setLastStartDate(response.data.data);
                                
                                if (oldCount != $scope.messages.length) {
                                    vm.scrollToMessagesTop();
                                }
                                
                            }, function (response) {

                    });
                };
                
                vm.setLastStartDate = function (messages) {
                    if (!messages || !messages[0]) {
                        return;
                    }
                    
                    var sortedDates = vm.getSortMessagesDates(messages);
                    vm.lastStartDate = sortedDates[0];
                };

                $scope.getMoreFutureMessages = function () {
                    if (!$scope.currentChat || !$scope.currentChat.id) {
                        return;
                    }
                    
                    chatService.getFutureMessages($scope.currentChat.id, vm.lastEndDate).then(
                            function (response) {
                                $scope.menuService.isShownChatsMenuForPhone = false;
                                
                                var oldCount = $scope.messages.length;
                                vm.addMessages(response.data.data);
                                
                                vm.setLastEndDate(response.data.data);
                                
                                if (oldCount != $scope.messages.length) {
                                    vm.scrollToMessagesBottom();
                                }
                                
                            }, function (response) {

                    });
                };
                
                vm.setLastEndDate = function (messages) {
                    if (!messages || !messages[0]) {
                        return;
                    }
                    
                    var sortedDates = vm.getSortMessagesDates(messages);
                    vm.lastEndDate = sortedDates[sortedDates.length-1];
                };
                
                vm.getSortMessagesDates = function (messages) {
                    var messageDates = messages.map(function (message) {
                        return message.createDate;
                    });
                    
                    messageDates = messageDates.sort();
                    
                    if (messageDates.length == 0) {
                        messageDates[0] = dateService.jsDateToRfcDate();
                    }
                    
                    return messageDates;
                };

                vm.showSuccessMessage = function (chat) {
                    if ($scope.currentExecutive.login) {
                        $scope.successMessage = "Пароль для сотрудника " + chat.name + " изменен усппешно";
                    } else {
                        $scope.successMessage = "Логин для сотрудника " + chat.name + " установлен усппешно";
                    }

                    $timeout(function () {
                        $scope.successMessage = null;
                    }, 5000);
                };

                vm.addMessages = function (messages) {
                    var messageIds = $scope.messages.map(function (message) {
                        return message.id
                    });
                    var index = 0;

                    for (var i = 0; i < messages.length; i++) {
                        index = messageIds.indexOf(messages[i].id);
                        if (index === -1) {
                            $scope.messages[$scope.messages.length] = messages[i];
                        } else {
                            $scope.messages[index] = messages[i];
                        }
                    }
                };

                $scope.onFocusEditor = function () {
                    if ($scope.newMessage.content == $scope.editorDefaultText) {
                        $scope.newMessage.content = "";
                    }
                };

                $scope.onBlurFocusEditor = function () {
                    if ($scope.newMessage.content == "") {
                        $scope.newMessage.content = $scope.editorDefaultText;
                    }
                };

                $scope.onKeyUpEditor = function ($event) {
                    if ($event.keyCode == vm.enterButtonKeyCode) {
                        $event.preventDefault();
                        vm.sendMessage();
                    } else {
                        vm.expandEditor($event);
                    }
                };

                $scope.uploadFile = function (fileElement) {
                    var file = fileElement.files[0];

                    if (!file.name) {
                        return;
                    }

                    chatService.uploadFile(file).then(
                            function (response) {

                            }, function (response) {

                        var newMessage = angular.copy($scope.newMessage);

                        newMessage.writeDate = "2017-08-31T05:58:42+00:00";
                        newMessage.type = 'file'
                        newMessage.file = file;
                        newMessage.authorId = $scope.currentUser.id;
                        newMessage.id = parseInt($scope.messages[$scope.messages.length - 1].id) + 1;
                        $scope.messages[$scope.messages.length] = newMessage;

                    });
                };

                $scope.clickOnFile = function ($event) {
                    angular.element($event.currentTarget).find('input')[0].click();
                };

                $scope.deleteMessage = function (message) {
                    if (!message || !message.id) {
                        return;
                    }

                    chatService.deleteMessage(message).then(
                            function (response) {

                            }, function (response) {

                        message.oldContent = angular.copy(message.content);
                        message.content = vm.deletedMessageText;
                        message.oldType = angular.copy(message.type);
                        message.type = "text";
                        message.isDeleted = true;
                    });
                };

                $scope.recoverMessage = function (message) {
                    if (!message || !message.id) {
                        return;
                    }

                    chatService.recoverMessage(message).then(
                            function (response) {

                            }, function (response) {

                        message.content = angular.copy(message.oldContent);
                        message.type = angular.copy(message.oldType);
                        message.isDeleted = false;
                    });
                };

                vm.sendMessage = function () {
                    $scope.newMessage.chatId = $scope.currentChat.id;

                    chatService.sendMessage($scope.newMessage).then(
                            function (newMessage) {
                                vm.addMessages([angular.copy(newMessage)]);
                                vm.scrollToMessagesBottom();

                                vm.clearChatMessage();

                            }, function (response) {

                    });
                };

                vm.jsDateToRFCDate = function (jsDate) {
                    var rruleSet = new RRuleSet();
                    rruleSet.rdate(jsDate);
                };

                vm.clearChatMessage = function () {
                    $scope.newMessage.content = "";
                };

                vm.expandEditor = function ($event) {
                    var editorTextWidth = vm.getEditorTextWidth($event, $event.target.value + "1");

                    var countRows = parseInt((editorTextWidth) / $event.target.offsetWidth) + 1;

                    if ($scope.editorCountRows != countRows && countRows <= vm.maxEditorCountRows) {
                        $scope.editorCountRows = countRows;
                    }
                };

                vm.getEditorTextWidth = function ($event, text) {
                    var style = window.getComputedStyle($event.target, null);
                    var fontSize = style.getPropertyValue('font-size');
                    var fontFamily = style.getPropertyValue('font-family');
                    var font = fontSize + " " + fontFamily;

                    var $div = angular.element(document.createElement("div"))

                    $div.text(text)
                            .css({'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font': font});

                    angular.element(document.getElementsByTagName("body")).append($div);

                    var width = $div[0].offsetWidth;

                    $div.remove();

                    return width;
                };

                vm.scrollToMessagesBottom = function () {
                    vm.scrollToMessages('bottom');
                };

                vm.scrollToMessagesTop = function () {
                    vm.scrollToMessages('top');
                };

                vm.scrollToMessages = function (tag) {
                    setTimeout(function () {
                        $location.hash(tag);
                        $anchorScroll();
                        $location.hash('');
                    }, 5000);
                };

                vm.init();
            }]);