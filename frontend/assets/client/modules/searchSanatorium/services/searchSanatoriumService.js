'use strict';
alenatApp.service("searchSanatoriumService", ['$api',
    function ($api) {
        var service = {};
        
        service.hash = 'search-sanatoria';

        service.getSearchSanatorium = function () {
           
            return $api.get(service.hash);
        };
        
        return service;
    }]);


