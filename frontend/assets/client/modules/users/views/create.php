
<div class="col-md-4 col-sm-12 align-center">
    <h1>Регистрация2</h1>

    <form role="form" name="regForm" ng-init="user.login = 'admin'">
        <div class= "form-group" ng-class="{error: regForm.login.$invalid}">
            <label> Логин </label>
            <div>
                <input name="login" ng-model="user.login" type= "text" ng-keyup="loginError = regForm.login.$dirty && regForm.login.$invalid" ng-blur="showLoginError = loginError" class="form-control" placeholder="Логин" disabled="disabled" required/>
                <div ng-show="showLoginError" class="alert alert-danger alert-error">
                    Логин обязателен
                </div>
            </div>
        </div>
        <div class= "form-group" ng-class="{error: regForm.password.$invalid}">
            <label> Пароль </label>
            <div>
                <input name="password" ng-model="user.password" type= "password" ng-keyup="passwordError = regForm.password.$dirty && !regForm.password.$invalid && !userService.isPasswordComplex(user.password)" ng-blur="showPasswordError = passwordError" class="form-control" placeholder="Пароль" required/>
                <small class="text-muted">8 знаков, символы, цифры, спец символы</small>
                <div ng-show="showPasswordError" class="alert alert-danger alert-error">
                    Укажите более сложный пароль
                </div>
                <div ng-show="regForm.password.$dirty && regForm.password.$invalid && !showPasswordError" class="alert alert-danger alert-error">
                    Пароль обязателен
                </div>
            </div>
        </div>
        <div class= "form-group" ng-class="{error: regForm.passwordTwice.$invalid}">
            <label> Повторите пароль </label>
            <div>
                <input ng-model="user.passwordTwice" type="password" ng-keyup="passwordTwiceError = regForm.password.$dirty && user.passwordTwice && !userService.isEaqualPasswords(user)" ng-blur="showPasswordTwiceError = passwordTwiceError" class="form-control" placeholder="Повтор пароля" required/>
                <div ng-show="showPasswordTwiceError" class="alert alert-danger alert-error">
                    Пароли не совпадают
                </div>
            </div>
        </div>
        <div ng-show="regErrorMessage && !showLoginError && !showPasswordError && !showPasswordTwiceError" class="alert alert-danger alert-error">
            Такой логин уже зарегистрирован
        </div>

        <div class="col-md-6 row col-sm-12">
            <button ng-click="createUser(user);" 
                    ng-disabled="regForm.$invalid || loginError || passwordError || passwordTwiceError"
                    type="submit" class="btn btn-default btn-block">Зарегистрироваться</button>
        </div>

    </form>
</div>

