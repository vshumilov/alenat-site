<div class="container col-md-11 align-center">
    <div class="col-md-4 col-sm-12 align-center">
        <h1>Вход</h1>

        <form role="form" name="authForm">
            <div class= "form-group" ng-class="{error: authForm.login.$invalid}">
                <label> Логин </label>
                <div>
                    <input name="login" ng-model="user.login" type= "text" ng-keyup="loginError = authForm.login.$dirty && authForm.login.$invalid" ng-blur="showLoginError = loginError" class="form-control" placeholder="Логин" required/>
                    <div ng-show="showLoginError" class="alert alert-danger alert-error">
                        Логин обязателен
                    </div>
                </div>
            </div>
            <div class= "form-group" ng-class="{error: authForm.password.$invalid}">
                <label> Пароль </label>
                <div>
                    <input name="password" ng-model="user.password" type= "password" ng-keyup="passwordError = authForm.password.$dirty && authForm.password.$invalid" ng-blur="showPasswordError = passwordError" class= "form-control" placeholder="Пароль" required/>
                    <div ng-show="showPasswordError" class="alert alert-danger alert-error">
                        Пароль обязателен
                    </div>
                </div>
            </div>
            <div ng-show="loginErrorMessage && !showLoginError && !showPasswordError" class="alert alert-danger alert-error">
                Неверный логин или пароль
            </div>

            <div class="col-md-4 row col-sm-12">
                <button ng-click="loginUser(user);" 
                        ng-disabled="authForm.$invalid || loginError || passwordError"
                        type="submit" class="btn btn-primary btn-block">Войти</button>
            </div>
        </form>
    </div>
</div>
