'use strict';
alenatApp.service("menuService", [
    function () {
        var service = {};
        
        service.showChatsMenuButton = function () {
            service.isShownChatsMenuButton = true;
        };
        
        service.hideChatsMenuButton = function () {
            service.isShownChatsMenuButton = false;
        };
        
        service.toggleChatsMenu = function () {
            service.isShownChatsMenuForPhone = !service.isShownChatsMenuForPhone;
        };

        return service;
    }]);


