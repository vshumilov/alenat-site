'use strict';
alenatApp.service("userService", ['authService', '$api', '$q',
    function (authService, $api, $q) {
        var service = {};

        service.anUpperCase = /[A-Z]/;
        service.aLowerCase = /[a-z]/;
        service.aNumber = /[0-9]/;
        service.aSpecial = /[!|@|#|$|%|^|&|*|(|)|-|_]/;
        service.minLength = 8;

        service.createUser = function (user) {
            var deferred = $q.defer();

            $api.post('users', user).then(
                    function (response) {
                        authService.setAuthorizedState(response.data.data.key, response.data.data.user);

                        deferred.resolve(response);
                    },
                    function (response) {
                        deferred.reject(response);
                    }
            );

            return deferred.promise;
        };

        service.loginUser = function (user) {
            var deferred = $q.defer();
            
            $api.post('users/login', {
                login: user.login,
                passwordHash: service.getPasswordHash(user)
            }).then(
                    function (response) {
                        authService.setAuthorizedState(response.data.data.key, response.data.data.user);

                        deferred.resolve(response);
                    },
                    function (response) {
                        deferred.reject(response);
                    }
            );

            return deferred.promise;
        };
        
        service.getPasswordHash = function (user) {
            if (!user || !user.login || !user.password) {
                return;
            }
            
            var login = user.login.toLowerCase();
            
            var stage1 = sha256(login + "sai4sahl]a=Yae+y" + user.password);
            
            var passwordHash = sha256("aicoh0Keew?ahv~a" + stage1);
            
            return passwordHash;
        };
        
        service.logoutUser = function () {
            var deferred = $q.defer();

            $api.post('users/logout').then(
                    function (response) {
                        authService.setUnauthorizedState();

                        deferred.resolve(response);
                    },
                    function (response) {
                        deferred.reject(response);
                    }
            );

            return deferred.promise;
        };

        service.isEaqualPasswords = function (user) {

            if (user && user.password && user.password == user.passwordTwice) {
                return true;
            }

            return false;
        };

        service.isPasswordComplex = function (password) {
            if (    !password ||
                    password.length < service.minLength || 
                    password.search(service.anUpperCase) == -1 ||
                    password.search(service.aLowerCase) == -1 || 
                    password.search(service.aNumber) == -1 || 
                    password.search(service.aSpecial) == -1)
            {
                return false;
            }

            return true;
        };

        service.prepareOptions = function (user) {
            if (!user.options) {
                user.options = [''];
            } else {
                user.options = JSON.parse(user.options);
            }

            return user;
        };

        return service;
    }]);


