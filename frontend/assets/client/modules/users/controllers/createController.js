'use strict';
alenatApp.controller('createUserController', ['$scope', '$location', 'authService', 'userService', '$api', 'chatService', 'menuService',
    function ($scope, $location, authService, userService, $api, chatService, menuService) {

        $scope.authService = authService;
        $scope.userService = userService;
        $scope.menuService = menuService;

        var vm = this;

        vm.init = function () {
            if (authService.isAuth()) {
                $location.path('/chats');
            }
        };

        $scope.createUser = function (user) {
            $scope.regErrorMessage = false;

            userService.createUser(user).then(
                    function (response) {

                        userService.loginUser(user).then(
                                function (response) {
                                    $scope.regErrorMessage = false;
                                    $location.path('/chats');
                                },
                                function (response) {
                                    $scope.regErrorMessage = true;
                                }
                        );
                    },
                    function (response) {
                        $scope.regErrorMessage = true;
                    }
            );
        }

        vm.init();
    }]);


