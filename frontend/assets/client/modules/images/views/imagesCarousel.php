<div uib-carousel active="active" interval="myInterval" no-wrap="noWrapSlides" template-url="modules/common/views/carousel.html">
    <div uib-slide ng-repeat="slide in slides track by slide.id" index="slide.id" class="sanatoriums-detail__slide-block">
        <img class="sanatoriums-detail__slide-img" style="background-image: url('{{slide.image}}')">
    </div>
</div>

