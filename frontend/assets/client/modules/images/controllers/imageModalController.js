'use strict';
alenatApp.controller('SanatoriumsImageModalController', [
    '$uibModalInstance', 'images', 'imageId', '$scope', 'imageService',
    function ($uibModalInstance, images, imageId, $scope, imageService) {
        var vm = this;
        vm.images = images;
        
        $scope.myInterval = null;
        $scope.noWrapSlides = false;
        $scope.active = imageId;
        var slides = $scope.slides = [];
        var currIndex = 0;

        $scope.addSlide = function () {
            slides.push({
                image: imageService.getMediumImageUrl(vm.images[currIndex]),
                text: vm.images[currIndex].name,
                id: currIndex++
            });
        };
        
        for (var i = 0; i < vm.images.length; i++) {
            $scope.addSlide();
        }
    }]);

