'use strict';
alenatApp.service("imageService", [ '$api',
    function ($api) {
        var service = {};
        
        service.hash = 'images';
        
        service.getList = function (parentId) {
            if (!parentId) {
                return;
            }

            return $api.get(service.hash, {parent_id: parentId});
        };

        service.getSmallImageUrl = function (thumbnail_name_s) {
            return '/images/thumbs/' + thumbnail_name_s;
        };
        
        service.getMediumImageUrl = function (image) {
            return '/images/thumbs/' + image.thumbnail_name_m;
        };

        return service;
    }]);


