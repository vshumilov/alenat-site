'use strict';
alenatApp.service("cityService", ['$api',
    function ($api) {
        var service = {};
        
        service.hash = 'cities';

        service.getCities = function (name) {
            if (!name) {
                return;
            }
            
            var params = {q: name};
            
            return $api.get(service.hash, params);
        };
        
        return service;
    }]);


