<div class="contacts-detail__background-block">
    <div class="contacts-detail__content-block">
        <div class="contacts-detail__info-block">
            <h1><?php echo $name ?></h1>
            <p>Юридический адрес: <?php echo $billing_address_street . ' ' . $billing_address_city . ' ' . $billing_address_state . ' ' . $billing_address_postalcode ?></p>
            <p>Фактический адрес: <?php echo $shipping_address_street . ' ' . $shipping_address_city . ' ' . $shipping_address_state . ' ' . $shipping_address_postalcode ?></p>
            <p>Телефоны: <?php echo $phone_office ?></p>
            <p>Эл. почта: <?php echo $email ?></p>
            <p><?php echo $nominative_position ?>: <?php echo $nominative_fio ?></p>
            <p>ИНН: <?php echo $inn ?></p>
            <p>КПП: <?php echo $kpp ?></p>
            <p>ОГРН: <?php echo $ogrn ?></p>
            <p>Описание: <?php echo $description ?></p>
        </div>
        <p>
            <?php echo $address_map ?></p>
        </p>
    </div>
</div>


