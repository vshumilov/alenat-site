'use strict';
alenatApp.service("countryService", ['$api',
    function ($api) {
        var service = {};
        
        service.hash = 'countries';

        service.getCountries = function (name) {
            if (!name) {
                return;
            }
            
            var params = {q: name};
            
            return $api.get(service.hash, params);
        };
        
        return service;
    }]);


