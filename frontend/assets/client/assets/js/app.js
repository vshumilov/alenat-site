
'use strict';

var alenatApp = angular.module('alenatApp', [
    'ngCookies',
    'ymaps',
    'ui.bootstrap.popover',
    'ui.bootstrap.carousel',
    'ui.bootstrap.modal',
    'ui.bootstrap.datepickerPopup',
    'ui.bootstrap.modal',
    'ui.bootstrap.dropdown',
    'ui.mask',
]).constant('SETTINGS', {
    AUTO_LOGOUT_TIMEOUT: 30000,
    AUTO_LOGOUT_TIMEOUT_POPUP: 30
}).constant('ROUTES', {
    PRELOGIN: '/',
    BASE_URL: top.location.origin + '/',
    API_URL: 'http://api.alenat.site/index.php/'
}).config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.withCredentials = true;
    }]);
