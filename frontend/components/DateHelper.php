<?php

namespace app\components;

class DateHelper
{

    public function dbDateToHumanDate($dbDate)
    {
        $date = new \DateTime($dbDate);
        return $date->format('d.m.Y');
    }
}
