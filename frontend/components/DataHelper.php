<?php

namespace app\components;

class DataHelper
{
    public $delimiter = ',';

    public function getIdsFromMultienum($multienumStr)
    {
        $ids = explode($this->delimiter, strtr($multienumStr, ['^' => '']));

        return $ids;
    }

    public function getIconUrl($iconName)
    {
        return '/modules/common/images/ic_' . $iconName . '.svg';
    }

    public function getImageUrl($thumbnailName)
    {
        if (empty($thumbnailName)) {
            return;
        }

        return '/images/thumbs/' . $thumbnailName;
    }

}
