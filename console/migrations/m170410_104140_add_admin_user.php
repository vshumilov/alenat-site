<?php

use yii\db\Migration;

class m170410_104140_add_admin_user extends Migration
{
    protected $password = '1';
    protected $tableName = 'user';


    public function safeUp()
    {
        $this->insert($this->tableName, [
            'username' => 'admin',
            'password_hash' => \Yii::$app->getSecurity()->generatePasswordHash($this->password),
            'auth_key' => substr(\Yii::$app->getSecurity()->generatePasswordHash($this->password), 0 , 32),
            'email' => 'vlad_shumilov1@mail.ru',
            'status' => '10',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);
        
        return true;
    }

    public function safeDown()
    {
        $query = "delete from `user` where `username` = admin";
        $command = Yii::app()->db->createCommand($query);
        $command->execute();

        return false;
    }
}
