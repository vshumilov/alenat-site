<?php

use yii\db\Migration;

class m170411_052707_create_languages extends Migration
{

    protected $tableName = 'language';
    protected $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'updated_at' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'deleted_at' => $this->dateTime()->null(),
            'is_deleted' => $this->boolean()->notNull()->defaultValue('0'),
            'name' => $this->string()->notNull()->unique(),
            'flag_url' => $this->string(),
            'description' => $this->string()->notNull(),
                ], $this->tableOptions);

        $this->createIndex('idx_' . $this->tableName . '_name', $this->tableName, 'name');

        $this->insert($this->tableName, [
            'name' => 'en_us',
            'description' => 'English'
        ]);

        $this->insert($this->tableName, [
            'name' => 'ru_ru',
            'description' => 'Russian'
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }

}
