<?php

use yii\db\Migration;
use common\models\Articlegroup;
use common\models\Article;

class m170411_055924_create_article extends Migration
{
    protected $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function safeUp()
    {
        $this->createTable(Article::tableName(), [
            'id' => $this->primaryKey(),
            'articlegroup_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'updated_at' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'deleted_at' => $this->dateTime()->null(),
            'is_deleted' => $this->boolean()->notNull()->defaultValue('0'),
            'name' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'file_url' => $this->text(),
            'file_name' => $this->string(),
            'file_ext' => $this->string(),
                ], $this->tableOptions);
        
        $this->addForeignKey(
                Articlegroup::tableName() . '_' . Article::tableName() . '_' . '_fk', Article::tableName(), 'articlegroup_id', Articlegroup::tableName(), 'id'
        );


        $this->createIndex('idx_' . Article::tableName() . '_name', Article::tableName(), 'name');
        
        $this->insert(Article::tableName(), [
            'articlegroup_id' => Articlegroup::find()->select('id')->where(['language' => 'en_us'])->asArray()->one()['id'],
            'name' => 'top_panel',
            'title' => 'Tactile Engineering',
            'description' => 'Home Features About technology Gallery Contacts'
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(Article::tableName());
    }

}
