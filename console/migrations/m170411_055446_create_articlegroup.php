<?php

use yii\db\Migration;
use common\models\Language;
use common\models\Articlegroup;

class m170411_055446_create_articlegroup extends Migration
{

    protected $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function safeUp()
    {
        $this->createTable(Articlegroup::tableName(), [
            'id' => $this->primaryKey(),
            'language' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'updated_at' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'deleted_at' => $this->dateTime()->null(),
            'is_deleted' => $this->boolean()->notNull()->defaultValue('0'),
            'name' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
                ], $this->tableOptions);

        $this->addForeignKey(
                Language::tableName() . '_' . Articlegroup::tableName() . '_' . '_fk', Articlegroup::tableName(), 'language', Language::tableName(), 'name'
        );

        $this->createIndex('idx_' . Articlegroup::tableName() . '_name', Articlegroup::tableName(), 'name');
        
        $this->createIndex('idx_' . Articlegroup::tableName() . '_language', Articlegroup::tableName(), 'language');

        $this->insert(Articlegroup::tableName(), [
            'name' => 'start_page',
            'title' => 'Start page',
            'language' => 'en_us'
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(Articlegroup::tableName());
    }

}
